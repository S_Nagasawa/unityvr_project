﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using Game.Interfaces;

namespace Game.Spawn 
{
    /// <summary>
    /// 継承した巡廻地点クラス
    /// </summary>
    public class WayPointSpawn : SpawnObjectBase
    {
        /// <summary>
        /// レンダラー
        /// </summary>
        public Renderer Renderer { get; private set; } 


        protected override void Awake ()
        {
            base.Awake ();
            this.Renderer = RootTransform.GetComponent<Renderer> () as MeshRenderer;
        }


        /// <summary>
        /// 通過した際に呼ばれる
        /// </summary>
        public override void UpdatePassed (bool isHasPassed)
        {
            base.UpdatePassed (isHasPassed);
            this.PassedUpdate ();
        }


        /// <summary>
        /// 通過後に何かする場合に処理
        /// </summary>
        protected override void PassedUpdate ()
        {
            if (this.Renderer == null) 
            {
                return;
            }
            this.Renderer.material.color = (IsHasPassed) ? Color.red : Color.blue;
        }
    }

}

