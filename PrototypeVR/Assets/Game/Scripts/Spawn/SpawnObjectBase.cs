﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using Game.Interfaces;

namespace Game.Spawn 
{

    /// <summary>
    /// Spawn object base.
    /// 巡廻地点自身の基底クラス
    /// </summary>
    public abstract class SpawnObjectBase : ObjectBehaviour 
    {

        /// <summary>
        /// 通過したかどうか
        /// </summary>
        public bool IsHasPassed { get; private set; }

        /// <summary>
        /// 通過した際に呼ばれる
        /// </summary>
        public virtual void UpdatePassed (bool isHasPassed)
        {
            this.IsHasPassed = isHasPassed;
        }

        /// <summary>
        /// 通過後に何かする場合に処理
        /// </summary>
        protected virtual void PassedUpdate () 
        {}
    }


}

