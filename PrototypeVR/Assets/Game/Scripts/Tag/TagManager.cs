﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Tag 
{

    /// <summary>
    /// タグを管理するManagerクラス
    /// </summary>
    public static class TagManager
    {
        public static readonly string SingleTonTag = "GameController";
        public static readonly string PlayerTag    = "Player";
        public static readonly string ActorTag     = "Actor";
        public static readonly string AISystemTag  = "AISystem";
    }


}

