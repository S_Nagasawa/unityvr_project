﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// 銃の発射口クラス
    /// WeaponComponentBaseのクラスに1つ以上持つ
    /// WeaponComponentの子供につける必要がある
    /// </summary>
    public class MuzzleController : ObjectBehaviour 
    {

        /// <summary>
        /// 親のWeaponクラス
        /// </summary>
        public WeaponComponentBase WeaponComponent { get; private set; }


        /// <summary>
        /// Power
        /// </summary>
        public float Power 
        {
            get { return WeaponComponent.WeaponModel.Power; }
        }


        /// <summary>
        /// ターゲット
        /// </summary>
        public Transform Target
        {
            get 
            {
                if (WeaponComponent.Parent.LockTarget == null) 
                {
                    return null;
                }
                return WeaponComponent.Parent.LockTarget.RootTransform; 
            }
        }


        /// <summary>
        /// WeaponのオーナーのTransform
        /// </summary>
        public Transform ParentRoot
        {
            get { return WeaponComponent.Parent.RootTransform; }
        }


        /// <summary>
        /// 有効な状態であるかどうか？
        /// </summary>
        private bool IsValidComponent ()
        {
            return WeaponComponent.Parent != null && 
                WeaponComponent.Parent.LockTarget != null;
        }


        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="weaponComponent">Weapon component.</param>
        public void Initialize (WeaponComponentBase weaponComponent)
        {
            WeaponComponent = weaponComponent;
            IsSetUpDone = true;
        }


        void Start ()
        {
            Observable.EveryGameObjectUpdate ()
            .Where (_ => IsValidComponent ())
            .Subscribe (_ => 
            {
                RootTransform.forward = WeaponComponent.RootTransform.TransformDirection (Vector3.forward);                        
            })
            .AddTo (this);
        }


        /// <summary>
        /// 座標を計算してShotを処理する
        /// </summary>
        /// <param name="component">Component.</param>
        /// <param name="bullet">Bullet.</param>
        public void ExecuteShot (GameObject bullet)
        {
            RootTransform.LookAt (Target);

            var start     = RootTransform.position;
            var position  = new Vector3 (RootTransform.forward.x * Power, RootTransform.forward.y, RootTransform.forward.z * Power);
            var container = BulletContainerController.Instance.RootTransform;

            {
                BulletComponentBase bulletComp = Extension.Generate<BulletComponentBase>(bullet, start, container);            
                bulletComp.Initialize (this);
                bulletComp.ExecuteShot (position);
                bulletComp.DoDestroy ();
            }
        }


        #if false
        void OnDrawGizmos ()
        {
            if (WeaponComponent.Parent == null || 
                WeaponComponent.Parent.LockTarget == null || 
                RootTransform == null) 
            {
                return;
            }
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(RootTransform.position, 0.04f);
        }
        #endif


    }


}
