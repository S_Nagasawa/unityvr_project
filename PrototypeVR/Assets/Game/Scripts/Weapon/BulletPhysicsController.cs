﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// PhysicsControllerBaseの派生クラス
    /// BulletComponentBaseに紐づく
    /// </summary>
    public class BulletPhysicsController : PhysicsControllerBase
    {

        public BulletComponentBase BulletComp
        {
            get { return Parent as BulletComponentBase; }
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void Initialize (ObjectBehaviour parent)
        {
            base.Initialize (parent);
            IsSetUpDone = true;
        }


        /// <summary>
        /// Raises the collision enter event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected override void OnCollisionEnter(Collision col) 
        {
            ObjectBehaviour _arg = col.gameObject.GetComponent<ObjectBehaviour> ();

            // dummy
            if (_arg is CharacterBehaviour) 
            {
                (_arg as CharacterBehaviour).ApplyTakenDamage (30);
            }
            Destroy (BulletComp.gameObject);
        }


    }


}

