﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// WeaponComponentBaseを継承したクラス
    /// </summary>
    public class RifleComponent : WeaponComponentBase
    {

        /// <summary>
        /// リロード中か？
        /// </summary>
        private bool _IsReloading = false;


        /// <summary>
        /// Bulletを消費する
        /// </summary>
        public override void Shot ()
        {
            base.Shot ();

            // MuzzleControllerがない
            if (CurrentMuzzle == null) 
            {
                throw new SystemException ("Current Bullet Root Null !!");
                return;
            }

            // 弾倉がない
            if (IsMagazineEmpty) 
            {
                Debug.Log ("<color=red> Can't shot Get to Resources !! </color>");
                return;
            }

            // リロード中
            if (_IsReloading) 
            {
                Debug.Log ("<color=yellow> Now Reloading... </color>");
                return;
            }

            // 玉がない
            if (IsBulletEmpty) 
            {
                Reload ();
                Debug.Log ("<color=lime> Reload Start </color>");
                return;
            }
                
            // Shot成功
            CurrentMuzzle.ExecuteShot(BulletPrefab);
            int bullet = WeaponModel.Amount;
            WeaponModel.UpdateAmount (bullet--);
        }


        /// <summary>
        /// 装填する
        /// </summary>
        public override void Reload ()
        {
            _IsReloading = true;
            StartCoroutine (ReloadRoutine(
            null, 
            () => 
            {
                // callbackでBulletをfuelにして、Magazineを消費する
                var magazine = WeaponModel.Magazine;
                WeaponModel.UpdateAmount(_CachedBullet);
                WeaponModel.UpdateMagazine(magazine--);
                _IsReloading = false;
            }));
        }


    }


}

