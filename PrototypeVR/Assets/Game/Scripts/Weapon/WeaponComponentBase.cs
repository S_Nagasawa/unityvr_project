﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// ItemBehaviourを継承したクラス
    /// </summary>
    public abstract class WeaponComponentBase : ItemBehaviour, IWeaponModel
    {

        [SerializeField][Tooltip("Bullet")]
        protected GameObject BulletPrefab;


        [SerializeField][Tooltip("装填完了までの時間")]
        protected float ReloadTime = 2f;


        [SerializeField][Tooltip("Gizmosの幅")]
        protected float Radius = 0.05f;


        [SerializeField][Tooltip("連発可能までのInterval")]
        protected float BulletInterval = 0.8f;


        /// <summary>
        /// 構えているか？
        /// </summary>
        public bool IsEquip { get; protected set; }


        /// <summary>
        /// 銃の発射口クラス
        /// </summary>
        public List<MuzzleController> MuzzleControllerList { get; protected set; }


        /// <summary>
        /// 単一の発射口クラス
        /// </summary>
        public MuzzleController CurrentMuzzle
        {
            get { return MuzzleControllerList.FirstOrDefault (); }
        }


        /// <summary>
        /// CastしたWeaponModel
        /// </summary>
        public WeaponModel WeaponModel 
        {
            get { return ItemModel as WeaponModel; }
        }


        /// <summary>
        /// 本来格納されている最大段数
        /// </summary>
        protected int _CachedBullet;


        /// <summary>
        /// 本来格納されている最大マガジン数
        /// </summary>
        protected int _CachedMagazine;


        /// <summary>
        /// 段数が空か？
        /// </summary>
        protected bool IsBulletEmpty
        {
            get { return WeaponModel.Amount <= 0; }
        }


        /// <summary>
        /// マガジンが空か？
        /// </summary>
        protected bool IsMagazineEmpty
        {
            get { return WeaponModel.Magazine <= 0; }
        }


        /// <summary>
        /// Weaponの力
        /// </summary>
        protected float Power
        {
            get { return WeaponModel.Power; }
        }


        /// <summary>
        /// 管理する親
        /// </summary>
        public CharacterBehaviour Parent { get; protected set; }


        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            base.Initialize ();
            CreateItemModel ();
            CreateBulletRoots ();
        }


        /// <summary>
        /// Character引数付きの初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public virtual void Initialize (CharacterBehaviour parent)
        {
            Parent = parent;
            CreateItemModel ();
            CreateBulletRoots ();
        }


        /// <summary>
        /// ItemModelを生成
        /// </summary>
        protected virtual void CreateItemModel ()
        {
            ItemModel = new WeaponModelFactory ().CreateDummy ();
            _CachedBullet   = WeaponModel.Amount;
            _CachedMagazine = WeaponModel.Magazine;
        }


        /// <summary>
        /// BulletRootsリストを生成する
        /// </summary>
        protected virtual void CreateBulletRoots ()
        {
            MuzzleControllerList = Extension.GetComponentsInChildrenWithoutSelf<MuzzleController> (RootTransform)
                .ToList();

            MuzzleControllerList
                .Where ((MuzzleController arg) => arg != null)
                .ToList ()
                .ForEach ((MuzzleController obj) => obj.Initialize (this));
        }


        /// <summary>
        /// 構える
        /// </summary>
        public virtual void Equip () 
        {
            IsEquip = true;
        }


        /// <summary>
        /// 解除する
        /// </summary>
        public virtual void UnEquip () 
        {
            IsEquip = false;
        }


        /// <summary>
        /// Bulletを消費する
        /// </summary>
        public virtual void Shot () 
        {
        }


        /// <summary>
        /// 装填する
        /// </summary>
        public virtual void Reload () 
        {
            
        }


        /// <summary>
        /// ReLoadJob
        /// </summary>
        public virtual IEnumerator ReloadRoutine (Nullable<float> interval, Action callback = null)
        {
            var total = interval.HasValue ? (float)interval : ReloadTime;
            var time  = 0.0f;
            while (time < total) 
            {
                time += Time.deltaTime;
                yield return null;
            }
            if (callback != null) 
            {
                callback ();
            }
            yield break;
        }


        /// <summary>
        /// 遅延ショット
        /// </summary>
        /// <returns>The shot routine.</returns>
        /// <param name="callback">Callback.</param>
        public virtual IEnumerator DelayShotRoutine (Action callback)
        {
            yield return ReloadRoutine (BulletInterval, callback);
        }


        #if false
        protected virtual void OnDrawGizmos ()
        {
            if (Muzzle == null || Parent == null) 
            {
                return;
            }
            Gizmos.color = Color.white;
            var position = Parent.RootTransform.position + Muzzle.localPosition;
            Gizmos.DrawWireSphere(position, Radius);
        }
        #endif


    }


}

