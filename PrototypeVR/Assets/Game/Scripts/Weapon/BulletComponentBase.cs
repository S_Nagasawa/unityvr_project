﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// Bulletの基底クラス
    /// Itemを継承
    /// </summary>
    public abstract class BulletComponentBase : ItemBehaviour
    {

        /// <summary>
        /// 剛体
        /// </summary>
        public Rigidbody Rigidbody { get; protected set; }


        /// <summary>
        /// 軌跡
        /// </summary>
        public Renderer TrailRenderer { get; protected set; }


        /// <summary>
        /// 発射口
        /// </summary>
        public MuzzleController MuzzleController { get; protected set; }


        /// <summary>
        /// 当たり判定のController
        /// </summary>
        public PhysicsControllerBase PhysicsController { get; protected set; }


        [SerializeField][Tooltip("Bulletの生存時間")]
        protected float LifeTime = 2.4f;


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parentItem">Parent item.</param>
        public virtual void Initialize (MuzzleController muzzleController)
        {
            MuzzleController  = muzzleController;
            ItemModel         = muzzleController.WeaponComponent.WeaponModel;
            Rigidbody         = RootTransform.GetComponent<Rigidbody> ();
            TrailRenderer     = RootTransform.GetComponentInChildren<Renderer> () as TrailRenderer;
            PhysicsController = RootTransform.GetComponent<PhysicsControllerBase> ();
            PhysicsController.Initialize (this); 
        }


        /// <summary>
        /// Shotを実行する
        /// </summary>
        public virtual void ExecuteShot () {}


        /// <summary>
        /// Shotを実行する
        /// </summary>
        public virtual void ExecuteShot (Vector3 position) {}


        /// <summary>
        /// LifeTimeのJobを実行する
        /// </summary>
        /// <returns>The time routine.</returns>
        protected virtual IEnumerator LifeTimeRoutine (System.Action callback = null)
        {
            yield return new WaitForSeconds (LifeTime);
            if (callback != null) 
            {
                callback ();                
            }
            yield break;
        }
    }


}
