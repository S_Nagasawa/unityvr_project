﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// BulletComponentBaseの派生クラス
    /// </summary>
    public class BulletComponent : BulletComponentBase
    {

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parentItem">Parent item.</param>
        /// <param name="muzzleController">Muzzle controller.</param>
        public override void Initialize (MuzzleController muzzleController)
        {
            base.Initialize (muzzleController);
            RootTransform.rotation = muzzleController.ParentRoot.rotation;
            IsSetUpDone = true;
        }


        /// <summary>
        /// Shotを実行する
        /// </summary>
        public override void ExecuteShot ()
        {
            base.ExecuteShot ();
        }


        /// <summary>
        /// Shotを実行する
        /// </summary>
        /// <param name="position">Position.</param>
        public override void ExecuteShot (Vector3 position)
        {
            if (Rigidbody == null) 
            {
                return;
            }
            Rigidbody.AddForce (position, ForceMode.Impulse);
        }

        #if false
        void Start ()
        {
            this.UpdateAsObservable()
            .Where(_ => IsSetUpDone)
            .FirstOrDefault()
            .Subscribe (_ => 
            {
                DoDestroy();
            })
            .AddTo (this);
        }
        #endif


        /// <summary>
        /// BehaviourをDestroyする
        /// </summary>
        public override void DoDestroy ()
        {
            Observable.FromCoroutine (() => LifeTimeRoutine ())
            .Subscribe (_ => 
            {
                Destroy (RootTransform.gameObject);
            })
            .AddTo (this);
        }


    }


}
