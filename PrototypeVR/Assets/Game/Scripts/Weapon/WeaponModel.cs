﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// ItemBaseの派生クラス
    /// </summary>
    public class WeaponModel : ItemBase
    {
        /// <summary>
        /// The init data.
        /// </summary>
        public static Dictionary<string, object> InitData = new Dictionary<string, object> () 
        {
            {"Name", "Rifle"},
            {"Amount", 100},
            {"Magazine", 100},
            {"Category", 3},
            {"Power", 60}
        };


        /// <summary>
        /// マガジン Amountを格納する
        /// </summary>
        public int Magazine { get; protected set; }


        /// <summary>
        /// Weaponの力
        /// </summary>
        public float Power { get; protected set; }


        /// <summary>
        /// マガジンを更新する
        /// </summary>
        /// <param name="magazine">Magazine.</param>
        public void UpdateMagazine (int magazine) 
        {
            Magazine = magazine;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Weapon.WeaponModel"/> class.
        /// </summary>
        public WeaponModel () : base () 
        {}


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Weapon.WeaponModel"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="magazine">Magazine.</param>
        public WeaponModel (string name, int amount, int magazine, byte category, float power) : base ()
        {
            Name     = name;
            Amount   = amount;
            Magazine = magazine;
            Category = category;
            Power    = power;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Weapon.WeaponModel"/> class.
        /// </summary>
        /// <param name="dictionary">Dictionary.</param>
        public WeaponModel (IDictionary dictionary) : base ()
        {
            if (dictionary.Contains ("Name")) 
            {
                Name = dictionary ["Name"].ToString ();
            }
            if (dictionary.Contains ("Amount")) 
            {
                Amount = int.Parse (dictionary ["Amount"].ToString ());
            }
            if (dictionary.Contains ("Magazine")) 
            {
                Magazine = int.Parse (dictionary ["Magazine"].ToString ());
            }
            if (dictionary.Contains ("Category")) 
            {
                Category = byte.Parse (dictionary ["Category"].ToString ());
            }
            if (dictionary.Contains ("Power")) 
            {
                Power = float.Parse (dictionary ["Power"].ToString ());
            }
        }

    }


    /// <summary>
    /// Weapon model factory.
    /// </summary>
    public class WeaponModelFactory 
    {
        public WeaponModel Create (string name, int amount, int magazine, byte category, float power)
        {
            return new WeaponModel (name, amount, magazine, category, power);
        }

        public WeaponModel CreateDummy ()
        {
            return new WeaponModel (WeaponModel.InitData);
        }
    }


}

