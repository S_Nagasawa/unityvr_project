﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AnimatorTools;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// 武器を操作するController
    /// </summary>
    public abstract class WeaponControllerBase : ObjectBehaviour, IWeaponModel
    {
        /// <summary>
        /// Shotが完了したら呼ばれる
        /// </summary>
        public virtual event System.Action OnShotCallback = delegate {};


        /// <summary>
        /// 親
        /// </summary>
        public ObjectBehaviour Parent { get; protected set; }


        /// <summary>
        /// 武器リスト
        /// </summary>
        public List<WeaponComponentBase> WeaponList { get; protected set; }


        /// <summary>
        /// 現在装備している武器
        /// </summary>
        public WeaponComponentBase CurrentWeapon { get; protected set; }


        public virtual bool IsEquip
        {
            get { return CurrentWeapon.IsEquip; }
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public virtual void Initialize (CharacterBehaviour parent)
        {
            Parent = parent;
        }


        /// <summary>
        /// Shot this instance.
        /// </summary>
        public virtual void Shot () {}


        /// <summary>
        /// 装填する
        /// </summary>
        public virtual void Reload () {}


        /// <summary>
        /// 構える
        /// </summary>
        public virtual void Equip () {}


        /// <summary>
        /// 解除する
        /// </summary>
        public virtual void UnEquip () {}
    }


}

