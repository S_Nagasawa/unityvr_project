﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AnimatorTools;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Weapon
{

    /// <summary>
    /// Weapon controller.の派生クラス
    /// </summary>
    public class WeaponController : WeaponControllerBase
    {

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void Initialize (CharacterBehaviour parent)
        {
            base.Initialize (parent);

            WeaponComponentBase[] items = Extension.GetComponentsInChildrenWithoutSelf<WeaponComponentBase> (parent.RootTransform);

            items
                .Where ((WeaponComponentBase arg) => arg != null)
                .ToList()
                .ForEach ((WeaponComponentBase arg) => arg.Initialize(parent));

            // @TODO とりあえずいっちゃん最初の
            WeaponList    = items.ToList ();
            CurrentWeapon = items.FirstOrDefault();
            IsSetUpDone   = true;
        }


        /// <summary>
        /// 構える
        /// </summary>
        public override void Equip ()
        {
            CurrentWeapon.Equip ();
        }


        /// <summary>
        /// 解除する
        /// </summary>
        public override void UnEquip ()
        {
            CurrentWeapon.UnEquip ();
        }


        /// <summary>
        /// Shot this instance.
        /// </summary>
        public override void Shot ()
        {
            StartCoroutine (CurrentWeapon.DelayShotRoutine (
                () => 
                { 
                    CurrentWeapon.Shot(); 
                }
            ));
        }


        /// <summary>
        /// 装填する
        /// </summary>
        public override void Reload ()
        {
            CurrentWeapon.Reload ();
        }

    }


}

