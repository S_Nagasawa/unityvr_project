﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using Game.Interfaces;

namespace Game.State
{
    public interface IStateObjectBase<TEnum> where TEnum : System.IConvertible
    {
        void ChangeState(TEnum state);
        bool IsCurrentState(TEnum state);
    }


    public abstract class StateObjectBase<T, TEnum> : CharacterBehaviour, IStateObjectBase<TEnum>
        where T : class where TEnum : System.IConvertible
    {

        protected List<State<T>> stateList = new List<State<T>>();

        protected StateMachine<T> stateMachine;

        public virtual void ChangeState(TEnum state)
        {
            if (this.stateMachine == null)
            {
                return;
            }
            this.stateMachine.ChangeState(this.stateList[state.ToInt32(null)]);
        }


        public virtual bool IsCurrentState(TEnum state)
        {
            if (this.stateMachine == null)
            {
                return false;
            }
            return this.stateMachine.CurrentState == this.stateList[state.ToInt32(null)];
        }


        protected virtual void Update()
        {
            if (this.stateMachine != null)
            {
                this.stateMachine.Update();
            }
        }
    }

}

