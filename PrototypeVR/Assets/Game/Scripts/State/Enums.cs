﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace Game.Enums
{

    /// <summary>
    /// AI、CharacterのActionパターン
    /// </summary>
    public enum ActorPattern
    {
        IDLE,           //アイドル
        WALK,           //歩く || 走る
        AIM,            //構える
        ATTACK,         //攻撃する
        PICK,           //物を取る
        CROUCH,         //しゃがむ
        JUMP,           //飛ぶ
        DIE,            //死ぬ
        EXPLODE,        //爆発
    }


    /// <summary>
    /// 各Sceneの流れのState
    /// </summary>
    public enum GameState
    {
        INTRO,      //始まり
        START,      //開始
        SCENARIO,   //シナリオ
        GAMEOVER,   //GameOver
        RESULT,     //クリア
    }


    /// <summary>
    /// AIが動く行動パターン
    /// </summary>
    public enum AISystemType
    {
        INVALID,            //無効なAIシステム
        WAYPOINT,           //巡廻AI
        FIND_TARGET,        //Target探索
        RESOURCE_TARGET,    //資源探索
    }


    /// <summary>
    /// uGUI Canvasの子供に着くタイプ
    /// </summary>
    public enum UIType
    {
        INVALID,    //未定義
        IMAGE,      //Image Component
        BUTTON,     //Button Component
        PANEL,      //Panel Component
        TEXT,       //Text Component
    }


    /// <summary>
    /// 歩兵種別
    /// </summary>
    public enum InfantryType : byte
    {
        INVALID   = 0,  //未定義
        ATTACKER,       //アタッカー
        COUNTER,        //カウンター
        DEFENDER,       //守備者
    }


    /// <summary>
    /// 距離種別
    /// </summary>
    public enum RangeType
    {
        INVALID,    //未定義
        LONG,       //長距離レンジ
        MIDDLE,     //中距離レンジ
        SHORT,      //短距離レンジ
    }


    /// <summary>
    /// アイテムの種別
    /// </summary>
    public enum ItemType : byte
    {
        INVALID = 0,//未定義
        MONEY,      //金
        RESOURCE,   //資源
        WEAPON,     //武器
    }


    /// <summary>
    /// Squadの種別
    /// Soldier、Robotなど...
    /// </summary>
    public enum SquadType : byte
    {
        INVALID = 0,//未定義
        SOLDIER,    //兵士
        ROBOT,      //ロボット
        GUARD,      //ガード
    }

}

