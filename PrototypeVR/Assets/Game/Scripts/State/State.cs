﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using Game.Actor.AI;
using Game.Actor.Systems;
using Game.Interfaces;
using Game.Spawn;

namespace Game.State
{

    /// <summary>
    /// FSMの基底クラス
    /// </summary>
    public class State<T>
    {
        // このステートを利用するインスタンス
        protected T owner;


        /// <summary>
        /// 経路探索システム
        /// </summary>
        public AISystemBase WayPointSystem { get; protected set; }

        /// <summary>
        /// Target探索システム
        /// </summary>
        public AISystemBase FindTargetSystem { get; protected set; }


        /// <summary>
        /// CharacterのWorld座標
        /// </summary>
        public virtual Vector3 Position { get; protected set; }


        /// <summary>
        /// PointerEnterが発火したかどうか？
        /// </summary>
        protected bool isGazenEntered = false;


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.State.State`1"/> class.
        /// </summary>
        /// <param name="owner">Owner.</param>
        public State(T owner)
        {
            this.owner = owner;
        }


        /// <summary>
        /// このステートに遷移する時に一度だけ呼ばれる
        /// </summary>
        public virtual void Enter()
        {
        }


        /// <summary>
        /// このステートである間、毎フレーム呼ばれる
        /// </summary>
        public virtual void Execute() 
        {
            
        }


        /// <summary>
        /// このステートから他のステートに遷移するときに一度だけ呼ばれる
        /// </summary>
        public virtual void Exit() 
        {
        }


        /// <summary>
        /// 到着したか
        /// </summary>
        protected virtual bool ArrivedAt ()
        {
            return false;
        }


        /// <summary>
        /// FindTargetが使える一定の距離かどうか？
        /// </summary>
        protected virtual bool CanChangeFindTarget ()
        {
            return (FindTargetSystem as FindTargetSystem).CanChangeFindTarget (Position);
        }


        /// <summary>
        /// ターゲットロスト判定
        /// </summary>
        /// <param name="hasResult">Has result.</param>
        /// <param name="otherActor">Other actor.</param>
        protected virtual void TryToFindTargetInSightOff (ref bool hasResult, ref CharacterBehaviour otherActor)
        {
        }


        /// <summary>
        /// 視野に入った際に強制的にWait処理をする
        /// </summary>
        protected virtual void OnGazenWaitEvent ()
        {
            if ((owner as CharacterBehaviour).IsApplyGazenEventEnter) 
            {
                if (!isGazenEntered) 
                {
                    isGazenEntered = true;
                    Observable.FromCoroutine (() => (owner as CharacterBehaviour).CharacterActionController.WaitRoutine ())
                    .Subscribe (_ => 
                    {
                        isGazenEntered = false;
                    });                    
                }
            }
        }


    }

}

