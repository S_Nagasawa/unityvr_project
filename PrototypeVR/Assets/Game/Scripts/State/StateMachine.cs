﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.State
{
    public class StateMachine<T>
	{
		private State<T> currentState;

		public StateMachine()
		{
			this.currentState = null;
		}

		public State<T> CurrentState
		{
			get { return this.currentState; }
		}

		public void ChangeState(State<T> state)
		{
			if (this.currentState != null)
			{
				this.currentState.Exit();
			}
			this.currentState = state;
			this.currentState.Enter();
		}

		public void Update()
		{
			if (this.currentState != null)
			{
				this.currentState.Execute();
			}
		}
	}


}

