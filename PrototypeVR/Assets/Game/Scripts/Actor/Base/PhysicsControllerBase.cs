﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using Game.Weapon;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor
{

    /// <summary>
    /// Colliderの当たり判定を管理する基底クラス
    /// </summary>
    public abstract class PhysicsControllerBase : ObjectBehaviour
    {

        /// <summary>
        /// BaseClass Collider
        /// </summary>
        public Collider Collider { get; protected set; }


        /// <summary>
        /// 管理する親クラス
        /// </summary>
        public ObjectBehaviour Parent { get; protected set; }


        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// CastしてComponentを返す
        /// </summary>
        /// <param name="arg">Argument.</param>
        public T GetColliderType <T>(ObjectBehaviour arg) where T : Component
        {
            return arg.RootTransform.GetComponent<Collider> () as T;
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public virtual void Initialize(ObjectBehaviour parent)
        {
            Parent = parent;
            if (Parent is CharacterBehaviour) 
            {
                Collider = ((CharacterBehaviour)Parent).CharacterActionController.Collider;
            }
            else
            {
                Collider = parent.RootTransform.GetComponent<Collider> ();
            }
        }


        /// <summary>
        /// Raises the trigger enter event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnTriggerEnter(Collider col) 
        {
            
        }


        /// <summary>
        /// Raises the trigger exit event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnTriggerExit(Collider col) 
        {
            
        }


        /// <summary>
        /// Raises the trigger stay event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnTriggerStay(Collider col) 
        {
            
        }


        /// <summary>
        /// Raises the collision enter event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnCollisionEnter(Collision col) 
        {
            
        }


        /// <summary>
        /// Raises the collision exit event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnCollisionExit(Collision col) 
        {
            
        }


        /// <summary>
        /// Raises the collision stay event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected virtual void OnCollisionStay(Collision col) 
        {
            
        }


    }


}

