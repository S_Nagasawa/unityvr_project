﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Weapon;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor
{

    /// <summary>
    /// PhysicsControllerBaseの派生クラス
    /// BulletComponentBaseに紐づく
    /// </summary>
    public class CharacterPhysicsController : PhysicsControllerBase
    {

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void Initialize (ObjectBehaviour parent)
        {
            base.Initialize (parent);
            IsSetUpDone = true;
        }


        /// <summary>
        /// Raises the collision enter event.
        /// </summary>
        /// <param name="col">Col.</param>
        protected override void OnCollisionEnter(Collision col) 
        {
            ItemBehaviour item = col.gameObject.GetComponent<ItemBehaviour>();

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (item != null) 
            {
                //Debug.Log (string.Format("<color=blue> CollisionItem:{0} </color>", item.gameObject));
            }
            #endif

            if (item is BulletComponentBase)
            {
                Destroy ((item as BulletComponentBase).gameObject);
            }
        }


    }


}

