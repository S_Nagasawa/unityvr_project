﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using UniRx;
using Game.Actor.Action;

namespace Game.Actor
{

    public interface IComponents
    {
        /// <summary>
        /// AIが持つAgentSystem
        /// </summary>
        NavMeshAgent Agent { get; }

        /// <summary>
        /// Animatorクラス
        /// </summary>
        Animator Animator { get; }

        /// <summary>
        /// 剛体
        /// </summary>
        Rigidbody Rigidbody { get; } 

        /// <summary>
        /// Collider
        /// </summary>
        Collider Collider { get; }

        /// <summary>
        /// ComponentsのSetup
        /// </summary>
        void SetUpComponents ();
    }


    public interface IGameComponents
    {
        /// <summary>
        /// 基本動作を管理するクラス
        /// </summary>
        CharacterActionControllerBase CharacterActionController { get; }
    }


}