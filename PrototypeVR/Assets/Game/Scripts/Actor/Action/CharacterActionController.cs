﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AnimatorTools;

using UniRx;
using UniRx.Triggers;



using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Action
{

    /// <summary>
    /// ActionControllerの継承クラス
    /// </summary>
    public class CharacterActionController : CharacterActionControllerBase
    {
        /// <summary>
        /// Actionが完了したら呼ばれる
        /// </summary>
        public override event System.Action OnTaskAction = delegate {};


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void Initialize (CharacterBehaviour parent)
        {
            base.Initialize (parent);
        }


        /// <summary>
        /// Agentを動かす、停止している場合は再開させる
        /// </summary>
        public override void SetDestinationHandler()
        {
            base.SetDestinationHandler();

            // @TODO Crouch, Jumpの判定をどこで行うか
            bool isCrouch = false;
            bool isJump   = false;

            if (Agent == null || Parent.LockTarget == null) 
            {
                return;
            }

            var isPassed   = (Parent.LockTarget is SpawnObjectBase) ? (Parent.LockTarget as SpawnObjectBase).IsHasPassed : false;            
            var position   = Parent.LockTarget.RootTransform.position;
            float distance = Vector3.Distance (RootTransform.position, position);

            Agent.SetDestination (position);

            // 一定の距離があり、通過していない場合
            if (distance > Agent.stoppingDistance && !isPassed) 
            {
                Move (Agent.desiredVelocity, isCrouch, isJump);
            } 
            else 
            {
                Move(Vector3.zero, isCrouch, isJump);
            }

            if (!IsTaskDone && distance < Agent.stoppingDistance) 
            {
                IsTaskDone = true;
                OnTaskAction ();
            }
        }


        /// <summary>
        /// Aimを開始する
        /// </summary>
        public override void AimDestinationHandler ()
        {
            base.AimDestinationHandler ();

            // @TODO Crouch, Jumpの判定をどこで行うか
            bool isCrouch = false;
            bool isJump   = false;

            if (Agent == null || Parent.LockTarget == null) 
            {
                return;
            }

            var isAlive = (Parent.LockTarget is CharacterBehaviour) ? (Parent.LockTarget as CharacterBehaviour).IsAlive () : false;

            if (!IsTaskDone && !isAlive) 
            {
                IsTaskDone = true;
                OnTaskAction ();
            }
        }


        /// <summary>
        /// Animatorの更新
        /// </summary>
        /// <param name="move">Move.</param>
        protected override void UpdateAnimator (Nullable<Vector3> move)
        {
            Vector3 position = move.HasValue ? (Vector3)move : Vector3.zero;

            {
                Animator.SetFloat(AnimatorManager.Foward, ForwardAmount, 0.1f, Time.deltaTime);
                Animator.SetFloat(AnimatorManager.Turn, TurnAmount, 0.1f, Time.deltaTime);
                Animator.SetBool (AnimatorManager.OnGround, IsGrounded);
                Animator.SetBool (AnimatorManager.Aiming, (Parent.LockTarget is CharacterBehaviour));
            }

            float runCycle = Mathf.Repeat(Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + RunCycleLegOffset, 1);
            float jumpLeg  = (runCycle < CircleHalf ? 1 : -1) * ForwardAmount;

            if (IsGrounded)
            {
                Animator.SetFloat(AnimatorManager.JumpLeg, jumpLeg);
            }
            if (IsGrounded && position.magnitude > 0)
            {
                Animator.speed = AnimSpeedMultiplier;
            }
            else
            {
                Animator.speed = 1;
            }
        }


        /// <summary>
        /// AnimatorComponentをoverrideする
        /// </summary>
        public override void OnAnimatorMove ()
        {

        }


    }

}

