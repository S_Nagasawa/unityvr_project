﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AnimatorTools;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Action
{

    /// <summary>
    /// ActionControllerの基底クラス
    /// </summary>
    public abstract class CharacterActionControllerBase : ObjectBehaviour, ICharacterAnimator, IComponents
    {
        /// <summary>
        /// Actionが完了したら呼ばれる
        /// </summary>
        public virtual event System.Action OnTaskAction = delegate {};

        /// <summary>
        /// タスクが完了したか
        /// </summary>
        public bool IsTaskDone { get; protected set; }

        /// <summary>
        /// 着地しているか
        /// </summary>
        public bool IsGrounded { get; protected set; }

        /// <summary>
        /// しゃがんでいるか？
        /// </summary>
        public bool IsCrouching { get; protected set; }

        /// <summary>
        /// AIが持つAgentSystem
        /// </summary>
        public NavMeshAgent Agent { get; protected set; }

        /// <summary>
        /// Animatorクラス
        /// </summary>
        public Animator Animator { get; protected set; }

        /// <summary>
        /// 剛体
        /// </summary>
        public Rigidbody Rigidbody { get; protected set; } 

        /// <summary>
        /// Collider
        /// </summary>
        public Collider Collider { get; protected set; }

        /// <summary>
        /// 親
        /// </summary>
        public CharacterBehaviour Parent { get; protected set; }

        /// <summary>
        /// WaitTimeのgetter
        /// </summary>
        public float ResumeWaitTime 
        {
            get { return WaitTime;  }
        }


        [Range(1f, 4f)]
        [SerializeField] protected float GravityMultiplier   = 2f;
        [SerializeField] protected float MovingTurnSpeed     = 360f;
        [SerializeField] protected float StationaryTurnSpeed = 180f;
        [SerializeField] protected float JumpPower           = 12f;
        [SerializeField] protected float RunCycleLegOffset   = 0.2f;
        [SerializeField] protected float MoveSpeedMultiplier = 1f;
        [SerializeField] protected float AnimSpeedMultiplier = 1f;
        [SerializeField] protected float GroundCheckDistance = 0.4f;
        [SerializeField] protected float HeadUpCheckDistance = 0.5f;
        [SerializeField] protected float WaitTime            = 4f;
        [SerializeField] protected float CrouchingHeight     = 1f;
        [SerializeField] protected float CrouchingRadius     = 0.3f;
        [SerializeField] protected Vector3 CrouchingCenter   = new Vector3 (0.06f, 0.5f, 0f);

        protected const float CircleHalf = 0.5f;

        protected float TurnAmount;
        protected float ForwardAmount;
        protected Vector3 GroundNormal;

        #region Original Size
        protected Vector3 Original_CapsuleCenter;
        protected float   Original_CapsuleHeight;
        protected float   Original_CapsuleRadius;
        #endregion


        [HideInInspector] 
        public int LayerMask = 1 << 8;//256


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public virtual void Initialize (CharacterBehaviour parent)
        {
            Parent = parent;
            SetUpComponents ();
        }


        /// <summary>
        /// Animator、Rigidbody、Colliderの初期化
        /// </summary>
        public virtual void SetUpComponents () 
        {           
            Animator  = RootTransform.GetComponent<Animator> ();
            Rigidbody = RootTransform.GetComponent<Rigidbody> ();
            Collider  = RootTransform.GetComponent<Collider> () as CapsuleCollider;
            LayerMask = ~LayerMask;
            Original_CapsuleHeight = (Collider as CapsuleCollider).height;
            Original_CapsuleCenter = (Collider as CapsuleCollider).center;
            Original_CapsuleRadius = (Collider as CapsuleCollider).radius;
            Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }


        /// <summary>
        /// Agentのセットアップ
        /// </summary>
        public virtual void SetUpAgent ()
        {
            Agent = RootTransform.GetComponent<NavMeshAgent> ();
            Agent.updateRotation  = false;
            Agent.updatePosition  = true;
            UpdateRigidBodyControll (true);
        }


        /// <summary>
        /// 待つJob
        /// </summary>
        /// <param name="callback">Callback.</param>
        public IEnumerator WaitRoutine (System.Action callback = null)
        {
            float time = Time.deltaTime;
            while (time < WaitTime) 
            {
                time += Time.deltaTime;
                yield return null;
            }
            if (callback != null) 
            {
                callback ();
            }
            yield return null;
        }


        /// <summary>
        /// AnimatorComponentをoverrideする
        /// </summary>
        public virtual void OnAnimatorMove () 
        {

        }


        /// <summary>
        /// 動く
        /// </summary>
        /// <param name="move">Move.</param>
        /// <param name="isCrouch">If set to <c>true</c> is crouch.</param>
        /// <param name="isJump">If set to <c>true</c> is jump.</param>
        public virtual void Move (Vector3 move, 
            bool isCrouch = false, 
            bool isJump = false)
        {
            if (move.magnitude > 1) 
            {
                move.Normalize ();
            }
            move = RootTransform.InverseTransformDirection(move);
            CheckGroundStatus();
            move = Vector3.ProjectOnPlane(move, GroundNormal);
            TurnAmount    = Mathf.Atan2(move.x, move.z);
            ForwardAmount = move.z;
            ApplyExtraTurnRotation (ForwardAmount, TurnAmount);

            if (IsGrounded)
            {
                HandleGroundedMovement (isCrouch, isJump);
            }

            ScaleCapsuleForCrouching (isCrouch);
            UpdateAnimator (move);
        }


        /// <summary>
        /// Agentを動かす、停止している場合は再開させる
        /// </summary>
        public virtual void SetDestinationHandler()
        {
            if (Agent == null) 
            {
                return;
            }
            Agent.Resume ();
            OnDraw ();
        }


        /// <summary>
        /// Agentを止める
        /// </summary>
        public virtual void StopDestinationHandler ()
        {
            if (Agent == null) 
            {
                return;
            }
            Agent.Stop ();
            Move (Vector3.zero);
        }


        /// <summary>
        /// Aimを開始する
        /// </summary>
        public virtual void AimDestinationHandler ()
        {
            if (Agent == null) 
            {
                return;
            }
            Agent.Stop ();
            Move (Vector3.zero);
            OnDraw ();
        }


        /// <summary>
        /// Agentに物理エンジンが競合しないように処理をする
        /// </summary>
        /// <param name="enable">If set to <c>true</c> enable.</param>
        public virtual void UpdateRigidBodyControll (bool enable)
        {
            Rigidbody.isKinematic = enable;
        }


        /// <summary>
        /// 着地判定処理
        /// </summary>
        protected virtual void CheckGroundStatus()
        {
            #if UNITY_EDITOR
            {
                Debug.DrawLine(RootTransform.position + (Vector3.up * 0.1f), RootTransform.position + (Vector3.up * 0.1f) + (Vector3.down * GroundCheckDistance), Color.red);
            }
            #endif

            RaycastHit hitInfo;
            if (Physics.Raycast(RootTransform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, GroundCheckDistance))
            {
                IsGrounded   = true;
                GroundNormal = hitInfo.normal;
                Animator.applyRootMotion = true;
            }
            else
            {
                IsGrounded   = false;
                GroundNormal = Vector3.up;
                Animator.applyRootMotion = false;
            }
        }


        /// <summary>
        /// 向きの制御処理
        /// </summary>
        /// <param name="forwardAmount">Forward amount.</param>
        /// <param name="turnAmount">Turn amount.</param>
        protected virtual void ApplyExtraTurnRotation(float forwardAmount, float turnAmount)
        {
            float turnSpeed = Mathf.Lerp(StationaryTurnSpeed, MovingTurnSpeed, forwardAmount);
            RootTransform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
        }


        /// <summary>
        /// Aiming時の制御処理
        /// Update等で使う
        /// </summary>
        public virtual void ApplyExtraTurnAiming ()
        {
            if (Parent.LockTarget == null) 
            {
                return;
            }
            Quaternion targetRotation = Quaternion.LookRotation(Parent.LockTarget.RootTransform.position - RootTransform.position);
            RootTransform.rotation    = Quaternion.Slerp(RootTransform.rotation, targetRotation, Time.deltaTime * StationaryTurnSpeed);            
        }


        /// <summary>
        /// ジャンプかしゃがみの判定
        /// </summary>
        /// <param name="isCrouch">If set to <c>true</c> is crouch.</param>
        /// <param name="isJump">If set to <c>true</c> is jump.</param>
        protected virtual void HandleGroundedMovement (
            bool isCrouch = false, 
            bool isJump = false)
        {
            if (isJump && !isCrouch && Animator.GetCurrentAnimatorStateInfo (0).IsName (AnimatorManager.Grounded)) 
            {
                Vector3 velocity    = new Vector3 (Rigidbody.velocity.x, JumpPower, Rigidbody.velocity.z);
                Rigidbody.velocity  = velocity;
                IsGrounded          = false;
                GroundCheckDistance = 0.1f;
                Animator.applyRootMotion = false;
            }
        }


        /// <summary>
        /// Raises the animator IK event.
        /// Params
        /// weight  (0-1) LookAt のグローバルウェイト、他のパラメーターの係数
        /// bodyWeight  (0-1) ボディが LookAt にどれぐらい関係するか決定
        /// headWeight  (0-1) 頭 (head)が LookAt にどれぐらい関係するか決定
        /// eyesWeight  (0-1) 目が LookAt にどれぐらい関係するか決定
        /// clampWeight (0-1) 0.0 の場合、キャラクターはモーションの制限が 0 であり、1.0 の場合、
        /// キャラクターは完全に制限され（ LookAt が不可能）、0.5 の場合、使用可能な範囲の半分（ 180 度）まで移動
        /// </summary>
        public virtual void OnAnimatorIK () 
        {
            
        }


        /// <summary>
        /// Animatorの更新
        /// </summary>
        /// <param name="move">Move.</param>
        protected virtual void UpdateAnimator(Nullable<Vector3> move)
        {
            
        }


        /// <summary>
        /// タスクの接続
        /// </summary>
        public virtual void ConnectTask ()
        {
            IsTaskDone = false;
        }


        /// <summary>
        /// Crouchするべき高さのGameObjectが付近に存在するか？
        /// </summary>
        protected virtual void PreventStandingInLowHeadroom ()
        {
            if (!IsCrouching)
            {
                CapsuleCollider col   = (Collider as CapsuleCollider);
                Ray crouchRay         = new Ray(Rigidbody.position + Vector3.up * col.radius * CircleHalf, Vector3.up);
                float crouchRayLength = Original_CapsuleHeight - col.radius * CircleHalf;

                if (Physics.SphereCast(crouchRay, col.radius * CircleHalf, crouchRayLength))
                {
                    /*
                    SetCrouchColliderSize();
                    IsCrouching = true;
                    */
                    return;
                }
                SetCrouchColliderSize(true);
                IsCrouching = false;
            }
        }

        /// <summary>
        /// Sets the size of the crouch collider.
        /// しゃがんだ時にcolliderの形状を変える
        /// </summary>
        protected virtual void SetCrouchColliderSize (bool reset = false)
        {
            CapsuleCollider col = (Collider as CapsuleCollider);
            col.height = reset ? Original_CapsuleHeight : CrouchingHeight;
            col.center = reset ? Original_CapsuleCenter : CrouchingCenter;
            col.radius = reset ? Original_CapsuleRadius : CrouchingRadius;
        }



        protected virtual void ScaleCapsuleForCrouching (bool crouch)
        {
            if (IsGrounded && crouch)
            {
                if (IsCrouching) 
                {
                    return;
                }
                SetCrouchColliderSize();
                IsCrouching = true;
                //StartCoroutine (_OnCrouchDown());
            }
            else
            {
                PreventStandingInLowHeadroom();
            }
        }


        /// <summary>
        /// 単純にDraw処理をする
        /// </summary>
        protected virtual void OnDraw ()
        {
            #if UNITY_EDITOR
            //Parent.TroopController.Update();

            if (Parent.LockTarget == null) 
            {
                return;
            }

            Color col = Color.cyan;

            if (Parent.LockTarget is CharacterBehaviour) 
            {
                col = Color.red;
            }

            var fromPos = RootTransform.position;
            var toPos   = Parent.LockTarget.RootTransform.position;

            Debug.DrawLine(fromPos, toPos, col);
            #endif
        }

    }


}

