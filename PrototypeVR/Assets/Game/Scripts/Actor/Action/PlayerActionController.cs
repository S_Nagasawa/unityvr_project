﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.FirstPerson;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Actor.Action
{

    /// <summary>
    /// ActionControllerの継承クラス
    /// </summary>
    [RequireComponent(typeof(Animator), typeof(Rigidbody), typeof(CapsuleCollider))]
    public class PlayerActionController : CharacterActionControllerBase
    {

        /// <summary>
        /// The fps controller.
        /// </summary>
        private FirstPersonBase fpsController;
        public FirstPersonBase FpsController 
        {
            get 
            { 
                if (fpsController == null) 
                {
                    fpsController = Parent.GetComponent<FirstPersonBase> () as FirstPersonController;
                }
                return  fpsController;
            }
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="parent">Parent.</param>
        public override void Initialize (CharacterBehaviour parent)
        {
            base.Initialize (parent);
        }


        /// <summary>
        /// 動く
        /// </summary>
        /// <param name="move">Move.</param>
        /// <param name="isCrouch">If set to <c>true</c> is crouch.</param>
        /// <param name="isJump">If set to <c>true</c> is jump.</param>
        public override void Move (Vector3 move, bool isCrouch = false, bool isJump = false)
        {
            base.Move (move, isCrouch, isJump);
        }


        /// <summary>
        /// ジャンプかしゃがみの判定
        /// </summary>
        /// <param name="isCrouch">If set to <c>true</c> is crouch.</param>
        /// <param name="isJump">If set to <c>true</c> is jump.</param>
        protected override void HandleGroundedMovement (
            bool isCrouch = false, 
            bool isJump = false)
        {
            if (isJump && !isCrouch) 
            {
                Vector3 velocity    = new Vector3 (Rigidbody.velocity.x, JumpPower, Rigidbody.velocity.z);
                Rigidbody.velocity  = velocity;
                IsGrounded          = false;
                GroundCheckDistance = 0.1f;
            }
        }


        /// <summary>
        /// 着地判定処理
        /// </summary>
        protected override void CheckGroundStatus()
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(RootTransform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, GroundCheckDistance))
            {
                IsGrounded   = true;
                GroundNormal = hitInfo.normal;
            }
            else
            {
                IsGrounded   = false;
                GroundNormal = Vector3.up;
            }
        }


        /// <summary>
        /// Handles the move.
        /// </summary>
        public void HandleMovement (bool isNeckDown)
        {
            if (FpsController == null) 
            {
                return;
            }

            CheckGroundStatus ();

            if (IsGrounded) 
            {
                // do something
            }

            // Cameraのlocal => worldPositionの変更
            FirstPersonController fps = (FpsController as FirstPersonController);
            Vector3 position   = (isNeckDown) ? Vector3.forward : Vector3.zero;
            Vector3 direction  = fps.CameraTransform.TransformDirection (position);
            Rigidbody.velocity = direction * (MoveSpeedMultiplier);
        }


        /// <summary>
        /// Handles the gaze event.
        /// </summary>
        /// <param name="arg">Argument.</param>
        public void HandleGazenEvent (ObjectBehaviour arg) 
        {
            if (arg == null ||
                arg.TriggerController == null) 
            {
                return;
            }

            var _lock = arg.TriggerController.IsLock;
            var _hold = arg.TriggerController.IsHold;

            if (_lock && !_hold) 
            {
                arg.ApplyGazenPointerEnter (Parent);             
            }
            if (_hold) 
            {
                arg.ApplyGazenPointerHold (Parent);                    
            }
        }


    }

}

