﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;


using UniRx;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Systems
{

    /// <summary>
    /// 距離やconditionで攻撃判定を行うtroopcontrollerの基底クラス
    /// Component継承ではない
    /// </summary>
    public abstract class TroopBase
    {
        /// <summary>
        /// 基準となる距離
        /// </summary>
        /// <value>The base range.</value>
        public virtual float BaseRange { get; protected set; }

        /// <summary>
        /// 中間レンジ
        /// </summary>
        public float MiddleRange { get { return BaseRange; } }

        /// <summary>
        /// 長距離レンジ
        /// </summary>
        public float LongRange { get { return BaseRange * 2f; } }

        /// <summary>
        /// 短距離レンジ
        /// </summary>
        public float ShortRange { get { return BaseRange * 0.5f; } }

        /// <summary>
        /// ActorList
        /// </summary>
        /// <value>The actors.</value>
        public virtual List<CharacterBehaviour> Actors { get; protected set; }


        /// <summary>
        /// 親のCharacterBehaviour
        /// </summary>
        public virtual CharacterBehaviour Parent { get; protected set; }


        /// <summary>
        /// 距離種別
        /// </summary>
        public RangeType CurrentRangeType { get; protected set; }


        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize ()
        {
        }


        /// <summary>
        /// 距離を更新する
        /// </summary>
        public virtual void Update ()
        {
        }


        /// <summary>
        /// ターゲットが一定の距離内か？
        /// </summary>
        public virtual bool FindTargetInSightOff (CharacterBehaviour actor)
        {
            return false;
        }


        /// <summary>
        /// 距離の種別をEnumで返却する
        /// </summary>
        public virtual RangeType GetRangeType ()
        {
            return RangeType.INVALID;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.TroopBase"/> class.
        /// </summary>
        public TroopBase ()
        {
        }
    }


    /// <summary>
    /// Troopbaseの派生クラス
    /// </summary>
    public class TroopController : TroopBase
    {
        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            base.Initialize ();
        }


        /// <summary>
        /// 距離を更新する
        /// </summary>
        public override void Update ()
        {
            if (Extension.IsNullOrEmpty<CharacterBehaviour> (Actors)) 
            {
                return;
            }

            foreach (CharacterBehaviour act in Actors) 
            {
                if (act == null) 
                {
                    continue;
                }
                var fromPos  = Parent.RootTransform.position;
                var toPos    = act.RootTransform.position;
                var distance = Vector3.Distance (fromPos, toPos);
                var col      = Color.white;

                // Middle Over Long Under
                if (distance < LongRange && distance > MiddleRange) 
                {
                    col = Color.green;
                }
                // Short Over Middle Undeer
                else if (distance < MiddleRange && distance > ShortRange)
                {
                    col = Color.yellow;
                }
                // Epsilon Over Short Under
                else if (distance < ShortRange && distance > float.Epsilon)
                {
                    col = Color.red;
                }

                if (col == Color.white) 
                {
                    return;
                }
                Debug.DrawLine (fromPos, toPos, col);
            }
        }


        /// <summary>
        /// ターゲットが一定の距離内か？
        /// </summary>
        public override bool FindTargetInSightOff (CharacterBehaviour actor)
        {            
            // 自身がAttackerじゃない場合は処理しない
            if (Parent.CharacterInfantryType != InfantryType.ATTACKER || actor == null)
            {
                return false;
            }

            var fromPos  = Parent.RootTransform.position;
            var toPos    = actor.RootTransform.position;
            var distance = Vector3.Distance (fromPos, toPos);

            // Middle Over Long Under
            if (distance < LongRange && distance > MiddleRange) 
            {
                return false;
            }
            // Short Over Middle Undeer
            else if (distance < MiddleRange && distance > ShortRange)
            {
                return true;
            }
            // Epsilon Over Short Under
            else if (distance < ShortRange && distance > float.Epsilon)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// 距離の種別をEnumで返却する
        /// </summary>
        public override RangeType GetRangeType ()
        {
            if (Parent == null || Parent.LockTarget == null || !Parent.IsAlive()) 
            {
                return RangeType.INVALID;
            }

            var fromPos  = Parent.RootTransform.position;
            var toPos    = Parent.LockTarget.RootTransform.position;
            var distance = Vector3.Distance (fromPos, toPos);

            // Middle Over Long Under
            if (distance < LongRange && distance > MiddleRange) 
            {
                return RangeType.LONG;
            }
            // Short Over Middle Undeer
            else if (distance < MiddleRange && distance > ShortRange)
            {
                return RangeType.MIDDLE;
            }
            // Epsilon Over Short Under
            else if (distance < ShortRange && distance > float.Epsilon)
            {
                return RangeType.SHORT;
            }

            return RangeType.INVALID;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.TroopController"/> class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="range">Range.</param>
        public TroopController (CharacterBehaviour parent, float range) : base ()
        {
            Parent    = parent;
            BaseRange = range;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.TroopController"/> class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="actors">Actors.</param>
        /// <param name="range">Range.</param>
        public TroopController (CharacterBehaviour parent, List<CharacterBehaviour> actors, float range) : base ()
        {
            Parent    = parent;
            Actors    = actors;
            BaseRange = range;
        }
    }


    /// <summary>
    /// Troop controller factory.
    /// </summary>
    public class TroopControllerFactory
    {

        public TroopController Create (CharacterBehaviour parent, float range)
        {
            return new TroopController (parent, range);
        }


        public TroopController Create (CharacterBehaviour parent, List<CharacterBehaviour> actors, float range)
        {
            return new TroopController (parent, actors, range);
        }
    }


}

