﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;


using UniRx;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Systems 
{
    /// <summary>
    /// 巡廻地点システム基底クラス
    /// </summary>
    public abstract class AISystemBase
    {
        public AISystemType AISystemType { get; protected set; }

        public SquadType OwnerSquadType { get; protected set; }

        /// <summary>
        /// システムが有効か
        /// </summary>
        public virtual bool IsValidSystem 
        {
            get { return false; }
        }

        /// <summary>
        /// 座標から最短距離のSpawnObjectを取得して返す
        /// </summary>
        public virtual ObjectBehaviour GetMinDistanceSpawnObject (Vector3 position)
        {
            return null;
        }


        /// <summary>
        /// システムの更新をする
        /// </summary>
        public virtual void SystemUpdate () 
        {
        }


        /// <summary>
        /// SquadTypeを更新する
        /// </summary>
        /// <param name="squadType">Squad type.</param>
        public virtual void UpdateSquadType (SquadType squadType)
        {
            OwnerSquadType = squadType;
        }


        /// <summary>
        /// システムの更新をする
        /// </summary>
        /// <param name="spawn">Spawn.</param>
        public virtual void SystemUpdate (SpawnObjectBase spawn)
        {
        }

        /// <summary>
        /// システムの更新をする
        /// </summary>
        /// <param name="arg">Argument.</param>
        public virtual void SystemUpdate (ObjectBehaviour arg)
        {
        }

        /// <summary>
        /// システムが全て完了しているか
        /// </summary>
        public virtual bool IsCompletedAll () 
        {
            return false;
        }


        public AISystemBase ()
        {
            this.AISystemType = AISystemType.INVALID;
        }
    }
        



    /// <summary>
    /// AI system base factory.
    /// </summary>
    public class AISystemBaseFactory
    {
        public WayPointSystem Create(List<SpawnObjectBase> wayPointList)
        {
            return new WayPointSystem(
                wayPointList, 
                AISystemType.WAYPOINT);
        }

        public FindTargetSystem Create (float remainingDistance, List<CharacterBehaviour> actorList)
        {
            return new FindTargetSystem(
                remainingDistance, 
                actorList,
                AISystemType.FIND_TARGET);
        }

    }

}

