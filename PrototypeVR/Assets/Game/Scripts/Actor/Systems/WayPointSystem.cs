﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Systems 
{



    /// <summary>
    /// 巡廻地点システム継承クラス
    /// </summary>
    public class WayPointSystem : AISystemBase
    {
        /// <summary>
        /// 巡廻地点リスト
        /// </summary>
        public List<SpawnObjectBase> WayPointList { get; private set; }


        /// <summary>
        /// システムが有効か
        /// </summary>
        public override bool IsValidSystem 
        {
            get { return !Extension.IsNullOrEmpty<SpawnObjectBase> (WayPointList); }
        }


        /// <summary>
        /// 座標から最短距離のSpawnObjectを取得して返す
        /// </summary>
        public override ObjectBehaviour GetMinDistanceSpawnObject(Vector3 position)
        {
            var range = Random.Range (0, 5);

            // 20%の確率でランダムに切り替え
            if (range == 0) 
            {
                var count = WayPointList.Count - 1;
                return WayPointList [Random.Range(0, count)];
            }

            return WayPointList
                .Where((SpawnObjectBase arg) => !arg.IsHasPassed)
                .OrderBy((SpawnObjectBase arg) => (arg.RootTransform.position - position).magnitude)
                .FirstOrDefault();
        }


        /// <summary>
        /// システムが全て完了しているか
        /// </summary>
        public override bool IsCompletedAll () 
        {
            return WayPointList
                .Where ((SpawnObjectBase arg) => arg != null)
                .All (((SpawnObjectBase arg) => arg.IsHasPassed));
        }


        /// <summary>
        /// システムの更新をする
        /// </summary>
        /// <param name="spawn">Spawn.</param>
        public override void SystemUpdate (SpawnObjectBase spawn)
        {
            //
        }


        /// <summary>
        /// システムの更新をする
        /// </summary>
        public override void SystemUpdate ()
        {
            foreach (SpawnObjectBase obj in WayPointList) 
            {
                if (obj == null) 
                {
                    continue;
                }
                obj.UpdatePassed (false);
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.WayPointSystem"/> class.
        /// </summary>
        /// <param name="wayPointList">Way point list.</param>
        /// <param name="systemType">System type.</param>
        public WayPointSystem(List<SpawnObjectBase> wayPointList, AISystemType systemType) : base()
        {
            WayPointList = wayPointList;
            AISystemType = systemType;
        }


        public override string ToString ()
        {
            return string.Format ("[WayPointSystemBehaviour: " +
                "WayPointListCount={0}, " +
                "IsValidSystem={1}]", 
                WayPointList.Count, 
                IsValidSystem);
        }
    }
        

}

