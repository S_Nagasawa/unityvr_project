﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.Systems 
{

    /// <summary>
    /// ターゲットを発見するシステムクラス
    /// </summary>
    public class FindTargetSystem : AISystemBase
    {

        /// <summary>
        /// システムが有効か
        /// </summary>
        public override bool IsValidSystem 
        {
            get { return !Extension.IsNullOrEmpty<CharacterBehaviour> (ActorList); }
        }


        public float RemainingDistance { get; private set; }

        /// <summary>
        /// CharacterList
        /// </summary>
        public List<CharacterBehaviour> ActorList { get; private set; }


        /// <summary>
        /// 座標から最短距離のSpawnObjectを取得して返す
        /// </summary>
        /// <param name="position">Position.</param>
        public override ObjectBehaviour GetMinDistanceSpawnObject (Vector3 position)
        {
            return ActorList
                .Where((CharacterBehaviour arg) => arg.IsAlive() && arg.SquadType != OwnerSquadType)
                .OrderBy((CharacterBehaviour arg) => (arg.RootTransform.position - position).magnitude)
                .FirstOrDefault();

        }


        /// <summary>
        /// FindTargetが使える一定の距離かどうか？
        /// </summary>
        public bool CanChangeFindTarget (Vector3 position)
        {
            if (GetMinDistanceSpawnObject (position) == null) 
            {
                return false;
            }
            CharacterBehaviour character = GetMinDistanceSpawnObject (position) as CharacterBehaviour;
            float _distance = Vector3.Distance (position, character.RootTransform.position);
            if (_distance < RemainingDistance) 
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.FindTargetSystem"/> class.
        /// </summary>
        /// <param name="remainingDistance">Remaining distance.</param>
        /// <param name="systemType">System type.</param>
        public FindTargetSystem (
            float remainingDistance, 
            AISystemType systemType) : base ()
        {
            RemainingDistance = remainingDistance;
            AISystemType      = systemType;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Actor.Systems.FindTargetSystem"/> class.
        /// </summary>
        /// <param name="remainingDistance">Remaining distance.</param>
        /// <param name="actorList">Actor list.</param>
        /// <param name="systemType">System type.</param>
        public FindTargetSystem (
            float remainingDistance, 
            List<CharacterBehaviour>actorList, 
            AISystemType systemType) : base ()
        {
            RemainingDistance = remainingDistance;
            ActorList         = actorList;
            AISystemType      = systemType;
        }


        public override string ToString ()
        {
            return string.Format ("[FindTargetSystem: " +
                "IsValidSystem={0}, " +
                "RemainingDistance={1}, " +
                "ActorListCount={2}]", 
                IsValidSystem, 
                RemainingDistance, 
                ActorList.Count);
        }
    }


}
