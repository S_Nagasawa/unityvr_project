﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Actor;
using Game.Spawn;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.AnimatorTools
{
    public static class AnimatorManager
    {
        public static readonly string Foward   = "Forward";
        public static readonly string Turn     = "Turn";
        public static readonly string Crouch   = "Crouch";
        public static readonly string OnGround = "OnGround";
        public static readonly string Jump     = "Jump";
        public static readonly string JumpLeg  = "JumpLeg";
        public static readonly string Grounded = "Grounded";
        public static readonly string Aiming   = "Aiming";

    }


}
