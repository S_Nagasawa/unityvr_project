﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using Game.Weapon;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Actor.AI 
{

    /// <summary>
    /// CharacterBehaviourの継承クラス
    /// Action（Move、Idle等）はActionControllerで行う
    /// </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterActionController))]
    [RequireComponent(typeof(WeaponController))]
    [RequireComponent(typeof(CharacterPhysicsController))]
    public class AICharacter : StateObjectBase<CharacterBehaviour, ActorPattern>, IFSMCharacter
    {       

        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// Singletonのシステム構築が完了しない場合を想定し
        /// 一定時間後WayPointSystemを実行する
        /// </summary>
        void Start ()
        {
            // 1 : 各種Componentの取得、Parameter初期化、Event初期化、各種デリゲードの設定
            this.Initialize ();


            // 2 : 
            // @TODO 動的にSpawnする場合はフラグが必要
            CharacterActionController.OnTaskAction += () => 
            {
                
            };


            // 3 : 自発AI起動 直列処理
            if (!Extension.IsNullOrEmpty<AISystemBase> (AISystemList)) 
            {
                return;
            }

            Observable
            .FromCoroutine (() => CharacterActionController.WaitRoutine())
            .SelectMany(CreateWayPointSystem)
            .SelectMany(CreateFindTargetSystem)
            .Subscribe (_ => 
            {
                ResultJobLog();
                FSMInitialize();
            });


            // 4 : LifeCycleJobの実行
            LifeCycleStream = Observable.EveryUpdate();
            LifeCycleStream
            .Where (_ => CharacterParameterModel != null && !IsAlive())
            .Take(1)
            .Subscribe (_ => 
            {
                // do something
                Destroy(RootTransform.gameObject);
            })
            .AddTo (this);

            // 5 : IsGrounded判定
            this.CharacterActionController
            .ObserveEveryValueChanged (_ => _.IsGrounded)
            .Subscribe (_ => 
            {
                //
            })
            .AddTo (this);
        }


        /// <summary>
        /// 巡廻地点システムを構築する
        /// </summary>
        protected virtual IEnumerator CreateWayPointSystem ()
        {
            SpawnObjectBase[] systems = GameObject.FindGameObjectsWithTag(TagManager.AISystemTag)
                .Select(((GameObject arg) => arg.GetComponent<SpawnObjectBase>()))
                .ToArray();

            // WayPointSystem生成
            AISystemList.Add(new AISystemBaseFactory().Create(systems.ToList()));
            yield break;
        }


        /// <summary>
        /// Character探索システムを構築する
        /// </summary>
        protected virtual IEnumerator CreateFindTargetSystem ()
        {
            List<CharacterBehaviour> actors = new List<CharacterBehaviour>();

            // 自分は除く *Spawnクラス管轄外のPlayerも除外
            foreach (CharacterBehaviour act in ActorSpawnController.Instance.ActorList) 
            {
                if (act == null || act == this) 
                {
                    continue;
                }
                actors.Add (act);
                yield return null;
            }

            // FindTargetSystem生成
            AISystemList.Add (new AISystemBaseFactory().Create(RemainingDistance, actors));

            // TroopController生成
            base.CreateTroopController(actors);
            yield break;
        }


        /// <summary>
        /// 各Propertyの初期化
        /// </summary>
        public override void Initialize ()
        {
            base.Initialize ();
            base.CreateParameterModel ();
            base.CreateActionControllerAgentSetUp ();
            base.CreateEventTrigger ();
            base.CreateWeaponController ();
            base.CreatePhysicsController ();
            base.AddDelegate();
            AISystemList = new List<AISystemBase> ();
            AITypeDict   = new Dictionary<AISystemType, ObjectBehaviour> ();
        }


        /// <summary>
        /// JOBが成功した時に呼ばれるlogメソッド
        /// </summary>
        protected override void ResultJobLog ()
        {
            if (Extension.IsNullOrEmpty<AISystemBase> (AISystemList)) 
            {
                IsSetUpDone = false;
                throw new SystemException (string.Format("Failed SetUpDone Instance:{0}", this));
            }

            // SquadTypeを更新する
            {
                AISystemList
                    .ForEach((AISystemBase arg) => arg.UpdateSquadType(SquadType));
            }

            IsSetUpDone = true;

            #if false
            Debug.Log(string.Format("<color=lime> Params:{0} </color>", CharacterParameterModel.ToString()));
            foreach (AISystemBase ai in AISystemList) 
            {
                Debug.Log(string.Format("<color=red> Character:{0} IsValid:{1} Type:{2} </color>",
                    this, 
                    ai.IsValidSystem, 
                    ai.AISystemType));
            }
            #endif
        }


        /// <summary>
        /// FSMシステム初期化
        /// </summary>
        public void FSMInitialize()
        {
            base.stateList.Add (new StateIdle(this));
            base.stateList.Add (new StateWalk(this));
            base.stateList.Add (new StateAim (this));
            base.stateMachine = new StateMachine<CharacterBehaviour> ();
            base.ChangeState (ActorPattern.IDLE);
        }


        /// <summary>
        /// FSMシステム削除
        /// </summary>
        public void FSMDestroy()
        {
            base.stateList.Clear ();
            base.stateMachine = null;
            SetLockTarget (null);
            RemoveDelegate ();
        }


        /// <summary>
        /// PointerEnter時に呼ばれる *Player引数付き
        /// </summary>
        /// <param name="arg">Argument.</param>
        public override void ApplyGazenPointerEnter (ObjectBehaviour arg)
        {
            if (IsApplyGazenEventEnter) 
            {
                return;
            }
            base.ApplyGazenPointerEnter ();

            #if false
            {
                Debug.Log (string.Format("<color=lime> AICharacter__ ApplyGazenPointerEnter:{0} </color>",
                    this));
            }
            #endif
        }


        /// <summary>
        /// PointerがHoldされている時に呼ばれる *Player引数付き
        /// </summary>
        /// <param name="arg">Argument.</param>
        public override void ApplyGazenPointerHold (ObjectBehaviour arg)
        {
            if (IsApplyGazenEventHold) 
            {
                return;
            }

            base.ApplyGazenPointerHold ();

            #if false
            {
                Debug.Log (string.Format("<color=lime> AICharacter__ ApplyGazenPointerHold:{0} </color>",
                    this));
            }
            #endif
        }


        /// <summary>
        /// Damageを与える
        /// </summary>
        public override void ApplyTakenDamage (int damageAmount)
        {
            CharacterParameterModel.ApplyTakenDamage (damageAmount);
        }



    }
}

