﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.FirstPerson;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;
using UIExtension = Game.Lib.UIBehaviourExtensions;

namespace Game.Events
{

    [RequireComponent (typeof(EventTrigger))]
    public class UIEventTriggerController : EventTriggerBase
    {
        
        public float LongPressTime { get; private set; }

        [SerializeField]
        private float _DefaultGazeTime = 2.4f;

        [SerializeField]
        private float _Interval = 0.0f;

        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            base.Initialize ();

            {
                UIExtension.AddListener (Trigger, EventTriggerType.PointerEnter, new UnityAction<BaseEventData> (OnPointerEnter));
                UIExtension.AddListener (Trigger, EventTriggerType.PointerDown, new UnityAction<BaseEventData> (OnPointerDown));
                UIExtension.AddListener (Trigger, EventTriggerType.PointerExit, new UnityAction<BaseEventData> (OnPointerExit));
            }
        }


        void Start ()
        {
            LongPressTime = _DefaultGazeTime;
            FirstPersonBase fps = FindObjectOfType<FirstPersonBase> ();
            if (fps != null) 
            {
                LongPressTime = fps.GazeHoldTime;
            }

            this.UpdateAsObservable ()
            .Subscribe (_ => 
            {
                HandleIntervalUpdate();
            })
            .AddTo (this);
        }



        private void HandleIntervalUpdate ()
        {
            if (IsLock) 
            {
                _Interval += Time.deltaTime;
                if (_Interval >= LongPressTime) 
                {
                    IsHold = true;
                }
            }
            else
            {
                IsHold   = false;
                _Interval = 0.0f;
            }
        }


        /// <summary>
        /// Raises the pointer enter event.
        /// </summary>
        /// <param name="data">Data.</param>
        public override void OnPointerEnter (BaseEventData data)
        {
            if (data == null) 
            {
                return;
            }
            #if false
            PointerEventData _data = data as PointerEventData;
            Debug.Log (string.Format("<color=yellow> PointerEnter Selected_UI:{0} </color>", _data));
            #endif
            IsLock = true;
        }


        /// <summary>
        /// Raises the pointer down event.
        /// </summary>
        /// <param name="data">Data.</param>
        public override void OnPointerDown (BaseEventData data)
        {
            if (data == null) 
            {
                return;
            }
        }


        /// <summary>
        /// Raises the pointer exit event.
        /// </summary>
        /// <param name="data">Data.</param>
        public override void OnPointerExit (BaseEventData data)
        {
            if (data == null) 
            {
                return;
            }
            IsLock = false;
            IsHold = false;
        }

    }



}


