﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
using Game.Interfaces;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.UI
{

    /// <summary>
    /// UIの継承クラス
    /// </summary>
    [RequireComponent(typeof(UIEventTriggerController))]
    public class UIComponent : UIObjectBehaviour
    {

        [SerializeField]
        private string _Name;

        [SerializeField]
        private UIObjectRootBehaviour _Root;

        [SerializeField]
        private UIType _UIType;

        [SerializeField]
        private CharacterBehaviour _LockTarget;


        protected override void Awake ()
        {
            base.Awake ();
            _Name  = UIObjectModel.Name;
            _Root  = RootCanvas;
            UIType = _UIType; // Inspectorの情報を反映する
        }


        void Start ()
        {
        }


        /// <summary>
        /// UIActionを実行する
        /// </summary>
        public override void OnUIAction ()
        {
            if (RootCanvas.LockTarget == null) 
            {
                return;
            }
            _LockTarget = RootCanvas.LockTarget;

            #if UNITY_EDITOR
            Debug.Log(string.Format("<color=yellow> OnUIAction_Call Component:{0} </color>", this));
            #endif
        }



    }


}
