﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Interfaces.UI;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.FirstPerson;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.UI
{


    /// <summary>
    /// ugui Canvasに紐づく派生クラス
    /// </summary>
    public class UICanvasController : UIObjectRootBehaviour
    {

        [SerializeField]
        private List<CharacterBehaviour> _TempCharacterList;

        [SerializeField][Tooltip("回転速度")]
        private float _Speed = 2f;


        protected override void Awake ()
        {
            base.Awake ();
            IsSetUpDone = false;
        }


        void Start ()
        {
            Initialize ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            if (Extension.IsNullOrEmpty<CharacterBehaviour> (Characters)) 
            {
                Observable.FromCoroutine(() => JobRoutine()).Subscribe(_ => { });
            }
            else
            {
                Debug.Log ("Aready Character List");
                IsSetUpDone = true;
            }
        }


        /// <summary>
        /// JOBを実行する
        /// </summary>
        protected override IEnumerator JobRoutine ()
        {
            Characters  = FindObjectsOfType<CharacterBehaviour> ().ToList();
            IsSetUpDone = true;

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            {
                _TempCharacterList = Characters;
            }
            #endif

            yield break;
        }


        /// <summary>
        /// Characterが一定の距離内か
        /// </summary>
        /// <param name="arg">Argument.</param>
        /// <returns><c>true</c> if this instance can gazen user interface event; otherwise, <c>false</c>.</returns>
        public override bool CanGazenUIEvent ()
        {
            LockTarget = GetMinDistanceCharacter();
            return base.CanGazenUIEvent ();
        }


        /// <summary>
        /// Canvasを回転させる
        /// </summary>
        public override void ApplyTurnRotation ()
        {
            #if UNITY_EDITOR
            {
                Debug.DrawLine(RootTransform.position, LockTarget.RootTransform.position, Color.yellow);
            }
            #endif

            Quaternion fromQue    = RootTransform.rotation;
            Quaternion toQue      = LockTarget.RootTransform.rotation;
            Quaternion initialQue = Quaternion.identity;

            // PlayerのみベクトルがCameraを基準としている為
            // Playerの場合はCameraのベクトルを参照 
            if (LockTarget is FirstPersonBase) 
            { 
                toQue = ((FirstPersonController)LockTarget).CameraTransform.rotation;
            }
            // InitialQueにy軸を代入する
            {
                initialQue.y = toQue.y;
            }
            RectTransform.rotation = Quaternion.Slerp (fromQue, initialQue, _Speed * Time.deltaTime);
        }


        /// <summary>
        /// 回転軸をリセットする
        /// </summary>
        /// <returns>The turn rotation.</returns>
        /// <param name="que">Que.</param>
        /// <param name="interval">Interval.</param>
        public override IEnumerator InitialTurnRotation (Quaternion que, float interval)
        {
            Quaternion fromQue = RootTransform.rotation;

            float time = 0.0f;
            while (time < interval) 
            {
                time += Time.deltaTime;
                yield return null;
            }

            RectTransform.rotation = Quaternion.Slerp (fromQue, que, time);
            yield break;
        }


    }



}

