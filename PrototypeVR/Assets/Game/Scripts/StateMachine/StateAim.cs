﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AI;
using Game.Tag;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.State.StateMachine 
{

    public class StateAim : State<CharacterBehaviour>
    {

        /// <summary>
        /// CharacterのWorld座標
        /// </summary>
        public override Vector3 Position 
        {
            get 
            {
                if (owner == null) 
                {
                    return Vector3.zero;
                }
                else
                {
                    return owner.RootTransform.position;
                }
            }
        }


        /// <summary>
        /// Cast to TroopController
        /// </summary>
        public TroopController TroopCtrl
        {
            get { return owner.TroopController as TroopController; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.State.StateMachine.StateAim"/> class.
        /// </summary>
        /// <param name="owner">Owner.</param>
        public StateAim (CharacterBehaviour owner) : base (owner)
        {
            WayPointSystem   = owner.GetAISystemOfType (AISystemType.WAYPOINT);
            FindTargetSystem = owner.GetAISystemOfType (AISystemType.FIND_TARGET);
        }


        /// <summary>
        /// このステートに遷移する時に一度だけ呼ばれる
        /// </summary>
        public override void Enter ()
        {
            base.Enter ();
            (owner as AICharacter).WeaponController.Equip ();
        }


        /// <summary>
        /// このステートである間、毎フレーム呼ばれる
        /// </summary>
        public override void Execute ()
        {
            if (!owner.IsAISystemAvailableAll) 
            {
                return;
            }

            // Rangeに応じて分ける
            switch (TroopCtrl.GetRangeType ()) 
            {
                case RangeType.MIDDLE:
                case RangeType.SHORT:
                {
                    owner.Aim();
                    ApplyTurnAiming ();
                }
                break;

                case RangeType.LONG:
                {
                    owner.Walk ();
                }
                break;

                case RangeType.INVALID:
                {
                    owner.SetLockTarget (null);
                    owner.ConnectTask ();
                    StateToIdle ();
                }
                break;
            }
        }


        /// <summary>
        /// プレイヤーの方向を向く
        /// </summary>
        private void ApplyTurnAiming ()
        {
            owner.CharacterActionController.ApplyExtraTurnAiming ();
        }


        private void StateToIdle ()
        {
            owner.Idle ();
            (owner as AICharacter).ChangeState (ActorPattern.IDLE);
        }


        /// <summary>
        /// このステートから他のステートに遷移するときに一度だけ呼ばれる
        /// </summary>
        public override void Exit ()
        {
            base.Exit ();
            (owner as AICharacter).WeaponController.UnEquip ();
        }


    }


}

