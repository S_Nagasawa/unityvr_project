﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AI;
using Game.Tag;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.State.StateMachine 
{

    public class StateWalk : State<CharacterBehaviour>
    {

        /// <summary>
        /// CharacterのWorld座標
        /// </summary>
        public override Vector3 Position 
        {
            get 
            {
                if (owner == null) 
                {
                    return Vector3.zero;
                }
                else
                {
                    return owner.RootTransform.position;
                }
            }
        }


        /// <summary>
        /// ActionControllerのタスクが完了しているか?
        /// </summary>
        public bool IsTaskDone 
        {
            get { return owner.CharacterActionController.IsTaskDone; }
        }


        /// <summary>
        /// Target Capture
        /// </summary>
        private bool _HasResult;


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.State.StateMachine.StateWalk"/> class.
        /// </summary>
        /// <param name="owner">Owner.</param>
        public StateWalk (CharacterBehaviour owner) : base (owner)
        {
            WayPointSystem   = owner.GetAISystemOfType(AISystemType.WAYPOINT);
            FindTargetSystem = owner.GetAISystemOfType (AISystemType.FIND_TARGET);
        }


        /// <summary>
        /// このステートに遷移する時に一度だけ呼ばれる
        /// </summary>
        public override void Enter ()
        {
            base.Enter ();

            {
                _HasResult     = false;
                isGazenEntered = false;
            }
        }


        /// <summary>
        /// このステートである間、毎フレーム呼ばれる
        /// </summary>
        public override void Execute ()
        {
            if (!owner.IsAISystemAvailableAll) 
            {
                return;
            }

            if (isGazenEntered) 
            {
                return;
            }
            base.OnGazenWaitEvent ();

            if (!_HasResult && owner.CharacterInfantryType == InfantryType.ATTACKER) 
            {
                CharacterBehaviour _otherActor = null;
                TryToFindTargetInSightOff (ref _HasResult, ref _otherActor);                
            }

            // LockTarget成功
            if (_HasResult) 
            {
                (owner as AICharacter).ChangeState (ActorPattern.AIM);
            }

            // ターゲットロスト idleステートへ切り替え
            if (owner.LockTarget == null) 
            {
                StateToIdle ();
            }

            // ターゲット地点に到着しているか
            if (this.ArrivedAt ()) 
            {
                // ターゲットを解除 idleステートへ切り替え
                owner.SetLockTarget (null);
                owner.ConnectTask ();
                StateToIdle();
            }
            else
            {
                owner.Walk ();
            }
        }


        /// <summary>
        /// ターゲットロスト判定
        /// </summary>
        /// <param name="hasResult">Has result.</param>
        protected override void TryToFindTargetInSightOff (ref bool hasResult, ref CharacterBehaviour otherActor)
        {
            otherActor = FindTargetSystem.GetMinDistanceSpawnObject (Position) as CharacterBehaviour;

            // CharacterをCaptureでき、且つ一定の距離の場合
            if (owner.TroopController.FindTargetInSightOff (otherActor)) 
            {
                hasResult = true;
                owner.SetLockTarget (otherActor);
            }
        }


        private void StateToIdle ()
        {
            owner.Idle ();

            Observable.FromCoroutine (() => owner.CharacterActionController.WaitRoutine ())
            .Subscribe(_ => 
            {
                //
            });
            (owner as AICharacter).ChangeState (ActorPattern.IDLE);
        }


        /// <summary>
        /// 到着したか
        /// </summary>
        protected override bool ArrivedAt ()
        {
            if (owner.LockTarget is SpawnObjectBase) 
            {
                return ((owner.LockTarget as SpawnObjectBase).IsHasPassed);
            }
            return false;
        }


        /// <summary>
        /// このステートから他のステートに遷移するときに一度だけ呼ばれる
        /// </summary>
        public override void Exit ()
        {
            base.Exit ();
        }


    }


}

