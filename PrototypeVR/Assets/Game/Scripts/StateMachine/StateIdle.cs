﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.AI;
using Game.Tag;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.State.StateMachine 
{

    public class StateIdle : State<CharacterBehaviour>
    {

        /// <summary>
        /// CharacterのWorld座標
        /// </summary>
        public override Vector3 Position 
        {
            get 
            {
                if (owner == null) 
                {
                    return Vector3.zero;
                }
                else
                {
                    return owner.RootTransform.position;
                }
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Game.State.StateMachine.StateIdle"/> class.
        /// </summary>
        /// <param name="owner">Owner.</param>
        public StateIdle (CharacterBehaviour owner) : base (owner)
        {
            WayPointSystem   = owner.GetAISystemOfType (AISystemType.WAYPOINT);
            FindTargetSystem = owner.GetAISystemOfType (AISystemType.FIND_TARGET);
        }


        /// <summary>
        /// このステートに遷移する時に一度だけ呼ばれる
        /// </summary>
        public override void Enter ()
        {
            owner.Idle ();
            base.Enter ();

            {
                isGazenEntered = false;
            }

            // AI巡回地点をリセットする
            if (WayPointSystem.IsCompletedAll ()) 
            {
                WayPointSystem.SystemUpdate ();
            }
        }


        /// <summary>
        /// このステートである間、毎フレーム呼ばれる
        /// </summary>
        public override void Execute ()
        {
            if (!owner.IsAISystemAvailableAll) 
            {
                return;
            }

            if (isGazenEntered) 
            {
                return;
            }
            base.OnGazenWaitEvent ();

            // ターゲットロスト判定
            ObjectBehaviour _arg           = null;
            CharacterBehaviour _otherActor = null;
            bool _hasResult                = false;
            TryToFindTargetInSightOff (ref _hasResult, ref _otherActor);

            // Capture失敗した場合はSpawnをCaptureし直す
            if (!_hasResult) 
            {
                _otherActor = null;
                _arg = WayPointSystem.GetMinDistanceSpawnObject(Position) as SpawnObjectBase;
                owner.SetLockTarget (_arg);
            }

            // ステート切替
            (owner as AICharacter).ChangeState ((_hasResult) ? ActorPattern.AIM : ActorPattern.WALK);
        }


        /// <summary>
        /// ターゲットロスト判定
        /// </summary>
        /// <param name="hasResult">Has result.</param>
        protected override void TryToFindTargetInSightOff (ref bool hasResult, ref CharacterBehaviour otherActor)
        {
            otherActor = FindTargetSystem.GetMinDistanceSpawnObject (Position) as CharacterBehaviour;

            // CharacterをCaptureでき、且つ一定の距離の場合
            if (owner.TroopController.FindTargetInSightOff (otherActor)) 
            {
                hasResult = true;
                owner.SetLockTarget (otherActor);
            }
        }

        /// <summary>
        /// このステートから他のステートに遷移するときに一度だけ呼ばれる
        /// </summary>
        public override void Exit ()
        {
            base.Exit ();
        }


    }


}

