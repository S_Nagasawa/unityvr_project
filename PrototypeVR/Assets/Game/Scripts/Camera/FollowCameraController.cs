﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.StaticMesh;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Cameras 
{


    public class FollowCameraController : CameraFollowBase
    {


        void Start ()
        {
            this.UpdateAsObservable ()
            .Where (_ => GvrGazePointerController.CurrentTarget != LockTarget)
            .Subscribe (_ => 
            {
                SetLockTarget(GvrGazePointerController.CurrentTarget);
            })
            .AddTo (this);
        }


        /// <summary>
        /// Cameraが捉えたObjectBehaviourを更新する
        /// Nullになる事もある
        /// </summary>
        /// <param name="arg">Argument.</param>
        protected override void SetLockTarget (ObjectBehaviour arg)
        {
            LockTarget = arg;

            if (LockTarget == null) 
            {
                return;
            }

            #if false
            Debug.Log(string.Format("<color=lime> " +
                "PointerPhysicsEvent__" +
                "Target::{0}, " +
                "</color>", 
                LockTarget.gameObject.name));
            #endif
        }



        public bool IsPlayerFocusingToUI()
        {
            Ray ray = new Ray(RootTransform.position, RootTransform.forward);
            return Physics.Raycast(ray, RayLength, LayerMask);
        }

    }


}

