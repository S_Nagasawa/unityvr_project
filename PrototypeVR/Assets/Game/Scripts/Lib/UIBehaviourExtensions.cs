﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Game.Lib
{

    public static partial class UIBehaviourExtensions 
    {


        public static void AddListener(this UIBehaviour uiBehaviour, EventTriggerType eventID, UnityAction<BaseEventData> callback)
        {
            var entry = new EventTrigger.Entry();
            entry.eventID = eventID;
            entry.callback.AddListener(callback);

            var eventTriggers = (uiBehaviour.GetComponent<EventTrigger>() ?? uiBehaviour.gameObject.AddComponent<EventTrigger>()).triggers;
            eventTriggers.Add(entry);
        }

        public static void RemoveAllListeners(this UIBehaviour uiBehaviour, EventTriggerType eventID)
        {
            var eventTrigger = uiBehaviour.GetComponent<EventTrigger>();

            if (eventTrigger == null)
                return;

            eventTrigger.triggers.RemoveAll(listener => listener.eventID == eventID);
        }


        public static void AddListener(this EventTrigger trigger, EventTriggerType eventID, UnityAction<BaseEventData> callback)
        {
            var entry = new EventTrigger.Entry();
            entry.eventID = eventID;
            entry.callback.AddListener(callback);

            var eventTriggers = trigger.triggers;
            eventTriggers.Add(entry);
        }


        public static void RemoveAllListeners(this EventTrigger trigger, EventTriggerType eventID)
        {
            var eventTrigger = trigger;

            if (eventTrigger == null)
                return;

            eventTrigger.triggers.RemoveAll(listener => listener.eventID == eventID);
        }


    }


}

