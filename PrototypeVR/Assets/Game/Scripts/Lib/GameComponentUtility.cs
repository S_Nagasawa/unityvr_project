﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.VR;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.UI;
using Game.Interfaces.Item;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.StaticMesh;
using Game.Weapon;

using UniRx;
using UniRx.Triggers;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Lib
{

    public static partial class GameComponentUtility 
    {

        /// <summary>
        /// コンテンツ毎にfilterしてListに追加
        /// </summary>
        /// <param name="interactiveContents">元のリスト</param>
        /// <param name="interactiveCharacters">Character基底クラス</param>
        /// <param name="statisticContents">StaticMesh基底クラス</param>
        /// <param name="uIContentsRoot">UICanvas基底クラス</param>
        public static void InteractiveContentsByFilter (
            List<ObjectBehaviour> interactiveContents,
            ref List<CharacterBehaviour> interactiveCharacters, 
            ref List<StaticObjectBehaviour> statisticContents, 
            ref List<UIObjectRootBehaviour> uIContentsRoot, 
            ref List<ItemBehaviour> interactiveItemList)
        {

            foreach (var contents in interactiveContents) 
            {
                if (contents == null || 
                    contents is UIObjectBehaviour || 
                    contents is CharacterActionControllerBase ||
                    contents is SpawnObjectBase ||
                    contents is MuzzleController ||
                    contents is PhysicsControllerBase) 
                {
                    continue;
                }
                else if (contents is CharacterBehaviour) 
                {
                    interactiveCharacters.Add (contents as CharacterBehaviour);
                }
                else if (contents is StaticObjectBehaviour) 
                {
                    statisticContents.Add (contents as StaticObjectBehaviour);
                }
                else if (contents is UIObjectRootBehaviour) 
                {
                    uIContentsRoot.Add (contents as UIObjectRootBehaviour);
                }
                else if (contents is ItemBehaviour)
                {
                    interactiveItemList.Add (contents as ItemBehaviour);
                }
                else
                {
                    #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Debug.Log (string.Format("<color=red> InValid_Type:{0} </color>", contents.GetType()));
                    #endif
                }
            }
            #if false
            {
                Debug.Log("<color=lime> InteractiveContentsByFilter-----Start----- </color>");
                {
                    foreach (CharacterBehaviour data in interactiveCharacters) 
                    {
                        Debug.Log (data.RootTransform.name);
                    }
                }
                {
                    foreach (StaticObjectBehaviour data in statisticContents) 
                    {
                        Debug.Log (data.RootTransform.name);
                    }
                }
                {
                    foreach (UIObjectRootBehaviour data in uIContentsRoot) 
                    {
                        Debug.Log (data.RootTransform.name);
                    }
                }
                Debug.Log("<color=lime> InteractiveContentsByFilter-----Etart----- </color>");
            }
            #endif

        }



    }


}
