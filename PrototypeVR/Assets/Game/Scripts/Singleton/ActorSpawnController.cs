﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Spawn;
using Game.Tag;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Singleton
{

    /// <summary>
    /// ActorをSpawnするcontroller
    /// </summary>
    public class ActorSpawnController : SingletonBase<ActorSpawnController>
    {

        /// <summary>
        /// SpawnFlowのルールクラス
        /// </summary>
        [System.Serializable]
        private class SpawnFlow
        {
            public int Count;
            public GameObject Actor;
        }


        /// <summary>
        /// SpawnFlowList
        /// </summary>
        [SerializeField]
        private List<SpawnFlow> _SpawnList = new List<SpawnFlow>();


        /// <summary>
        /// SpawnしたActorを追加するList
        /// </summary>
        [SerializeField] 
        private List<CharacterBehaviour> _ActorList = new List<CharacterBehaviour>();


        public List<CharacterBehaviour> ActorList 
        {
            get { return _ActorList; }
        }


        [SerializeField]
        float _FieldMinX = 1f;

        [SerializeField]
        float _FieldMaxX = 100f;

        [SerializeField]
        float _FieldMinZ = 1f;

        [SerializeField]
        float _FieldMaxZ = 100f;

        private int _SpawnTotalCount;


        protected override void Awake ()
        {
            base.Awake ();
            _SpawnTotalCount = 0;
        }


        /// <summary>
        /// 初期化JOB
        /// </summary>
        public override IEnumerator Initializer ()
        {
            yield return base.Initializer ();
        }


        /// <summary>
        /// 成功した時に呼ばれる
        /// </summary>
        public override void SuccessCallback ()
        {
        }


        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public override void SystemCreatedCheck ()
        {
            IsSetUpDone = (ActorList.Count == _SpawnTotalCount);

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (!IsSetUpDone || Instance == null) 
            {
                Debug.Log(string.Format("<color=red> {0}_FailJob </color>", Instance));
            }
            else
            {
                Debug.Log(string.Format("<color=cyan> {0}_SuccessJob </color>", Instance));                          
            }
            #endif
        }


        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        /// <returns>The routine.</returns>
        public override IEnumerator JobRoutine ()
        {
            if (Extension.IsNullOrEmpty<SpawnFlow>(_SpawnList)) 
            {
                yield break;
            }
            for (int index = 0; index < _SpawnList.Count; index++) 
            {
                SpawnFlow flow = _SpawnList [index];
                for (int i = 0; i < flow.Count; i++) 
                {
                    var prefab   = flow.Actor;
                    var position = new Vector3 (Random.Range (_FieldMinX, _FieldMaxX), 0f, Random.Range(_FieldMinZ, _FieldMaxZ));
                    CharacterBehaviour go = Extension.Generate<CharacterBehaviour>(prefab, position, RootTransform);
                    _ActorList.Add (go);
                    _SpawnTotalCount++;
                }
                yield return null;
            }
            yield break;
        }

    }


}
