﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Spawn;
using Game.Tag;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Singleton
{

    /// <summary>
    /// 巡回地点をdeployするcontroller
    /// </summary>
    public class WayPointSystemController : SingletonBase<WayPointSystemController>
    {

        /// <summary>
        /// Spawnする巡廻地点
        /// </summary>
        [SerializeField] 
        private GameObject spawnWayPointPrefab;

        /// <summary>
        /// SpawnしたSystemを追加するList
        /// </summary>
        [SerializeField] 
        private List<SpawnObjectBase> spawnList = new List<SpawnObjectBase>();

        /// <summary>
        /// Spawnする数
        /// </summary>
        [Range(1f, 100f)][SerializeField] 
        private int spawnCount = 6;

        [SerializeField]
        float fieldMinX = 1f;

        [SerializeField]
        float fieldMaxX = 100f;

        [SerializeField]
        float fieldMinZ = 1f;

        [SerializeField]
        float fieldMaxZ = 100f;


        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// 初期化JOB
        /// </summary>
        public override IEnumerator Initializer ()
        {
            yield return base.Initializer ();
        }


        /// <summary>
        /// 成功した時に呼ばれる
        /// </summary>
        public override void SuccessCallback ()
        {
        }


        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public override void SystemCreatedCheck ()
        {
            IsSetUpDone = (spawnList.Count == spawnCount);

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (!IsSetUpDone) 
            {
                Debug.Log(string.Format("<color=red> {0}_FailJob </color>", Instance));
            }
            else
            {
                Debug.Log(string.Format("<color=cyan> {0}_SuccessJob </color>", Instance));                          
            }
            #endif
        }


        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        /// <returns>The routine.</returns>
        public override IEnumerator JobRoutine ()
        {
            if (this.spawnWayPointPrefab == null) 
            {
                yield break;
            }
            for (int index = 0; index < this.spawnCount; index++) 
            {
                var position = new Vector3 (Random.Range (fieldMinX, fieldMaxX), 0f, Random.Range(fieldMinZ, fieldMaxZ));
                SpawnObjectBase go = Extension.Generate<SpawnObjectBase>(this.spawnWayPointPrefab, position, RootTransform);
                this.spawnList.Add (go);
            }
            yield return null;
        }

    }


}
