﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Spawn;
using Game.Tag;
using UniRx;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Singleton
{

    /// <summary>
    /// Bullet_Shellの親となるSingleton Controller
    /// 現時点では用途はあまりない
    /// </summary>
    public class BulletContainerController : SingletonBase<BulletContainerController>
    {

        protected override void Awake ()
        {
            base.Awake ();
        }

    }

}

