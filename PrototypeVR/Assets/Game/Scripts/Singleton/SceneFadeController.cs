﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Spawn;
using Game.Tag;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Singleton
{

    /// <summary>
    /// Scene切り替え時のFadeController
    /// </summary>
    public class SceneFadeController : SingletonBase<SceneFadeController>
    {

        /// <summary>
        /// 暗転用黒テクスチャ
        /// </summary>
        private Texture2D _BlackTexture;
        public Texture2D BlackTexture 
        {
            get 
            {
                if (_BlackTexture == null) 
                {
                    _BlackTexture = new Texture2D (32, 32, TextureFormat.RGB24, false);
                }
                return _BlackTexture;
            }
        }


        /// <summary>
        /// フェード中の透明度
        /// </summary>
        private float _FadeAlpha = 0;


        /// <summary>
        /// フェード中かどうか
        /// </summary>
        private bool _IsFading = false;


        /// <summary>
        /// Scene切り替え時間
        /// </summary>
        private float _Interval = 1.4f;


        protected override void Awake ()
        {
            base.Awake ();
        }


        public void OnGUI ()
        {
            if (!_IsFading) 
            {
                return;
            }
            GUI.color = new Color (0, 0, 0, _FadeAlpha);
            GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), BlackTexture);
        }


        /// <summary>
        /// 画面遷移
        /// </summary>
        /// <param name='sceneName'>シーン名</param>
        /// <param name='interval'>暗転にかかる時間(秒)</param>
        public void LoadLevel(string sceneName, float? interval)
        {
            float time = (interval.HasValue) ? interval.Value : _Interval;
            Observable.FromCoroutine (() => JobRoutine (sceneName, time))
            .Subscribe (_ => 
            {
                IsSetUpDone = true;
                SystemCreatedCheck();
            });
        }


        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public override void SystemCreatedCheck ()
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (!IsSetUpDone) 
            {
                Debug.Log(string.Format("<color=red> {0}_FailJob </color>", Instance));
            }
            else
            {
                Debug.Log(string.Format("<color=cyan> {0}_SuccessJob </color>", Instance));                          
            }
            #endif
        }


        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        /// <param name="interval">Interval.</param>
        public override IEnumerator JobRoutine (string sceneName, float interval)
        {
            // Texture Renderer
            {
                yield return new WaitForEndOfFrame();
                BlackTexture.ReadPixels (new Rect (0, 0, 32, 32), 0, 0, false);
                BlackTexture.SetPixel (0, 0, Color.white);
                BlackTexture.Apply ();
            }

            // FadeOut
            _IsFading = true;
            float time = 0;
            while (time <= interval) 
            {
                _FadeAlpha = Mathf.Lerp (0f, 1f, time / interval);      
                time += Time.deltaTime;
                yield return null;
            }

            // Load Scene
            yield return StartCoroutine(SceneJobRoutine(sceneName));

            // FadeIn
            time = 0;
            while (time <= interval) 
            {
                _FadeAlpha = Mathf.Lerp (1f, 0f, time / interval);
                time += Time.deltaTime;
                yield return null;
            }
            _IsFading = false;
        }


        /// <summary>
        /// Scenes the job routine.
        /// </summary>
        /// <returns>The job routine.</returns>
        /// <param name="sceneName">Scene name.</param>
        private IEnumerator SceneJobRoutine (string sceneName)
        {
            AsyncOperation async = SceneManager.LoadSceneAsync (sceneName);
            async.allowSceneActivation = false;
            Resources.UnloadUnusedAssets();


            while (async.progress < 0.9f) 
            {
                #if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.Log(string.Format("<color=cyan> Progress:{0} </color>", async.progress));
                #endif

                #if false
                loadingText.text = (async.progress * 100).ToString("F0") + "%";
                loadingBar.fillAmount = async.progress;
                #endif
                yield return new WaitForEndOfFrame();
            }

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.Log(string.Format("<color=cyan> Scene Loaded </color>"));
            #endif

            #if false
            loadingText.text = "100%";
            loadingBar.fillAmount = 1;
            #endif
            yield return new WaitForSeconds(1);
            async.allowSceneActivation = true;
            yield break;
        }


    }


}
