﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using UniRx;

namespace Game.Singleton
{

    /// <summary>
    /// Singleton.
    /// </summary>
    public class SingletonBase<T> : SingletonBehaviour where T : SingletonBehaviour
    {

        /// <summary>
        /// インスタンス
        /// </summary>
        private static volatile T instance;

        /// <summary>
        /// 同期オブジェクト
        /// </summary>
        private static object syncObj = new object (); 

        /// <summary>
        /// インスタンスのgetter/setter
        /// </summary>
        public static T Instance 
        {
            get 
            {
                // アプリ終了時に，再度インスタンスの呼び出しがある場合に，オブジェクトを生成することを防ぐ
                if(applicationIsQuitting) 
                {
                    return null;
                }
                // インスタンスがない場合に探す
                if(instance == null) 
                {
                    instance = FindObjectOfType<T>() as T;

                    // 複数のインスタンスがあった場合
                    if ( FindObjectsOfType<T>().Length > 1 ) 
                    {
                        return instance;
                    }

                    // Findで見つからなかった場合、新しくオブジェクトを生成
                    if (instance == null) 
                    {
                        // 同時にインスタンス生成を呼ばないためにlockする
                        lock (syncObj) 
                        { 
                            GameObject singleton = new GameObject();
                            // シングルトンオブジェクトだと分かりやすいように名前を設定
                            singleton.name = typeof(T).ToString() + " (singleton)";
                            instance = singleton.AddComponent<T>();
                            // シーン変更時に破棄させない
                            DontDestroyOnLoad(singleton);
                        }
                    }
                }
                return instance;
            }
            // インスタンスをnull化するときに使うのでprivateに
            private set 
            {
                instance = value;
            }
        }

        /// <summary>
        /// アプリが終了しているかどうか
        /// </summary>
        static bool applicationIsQuitting = false;


        /// <summary>
        /// Applicationを終了する
        /// </summary>
        void OnApplicationQuit() 
        {
            applicationIsQuitting = true;
        }


        /// <summary>
        /// Instanceを削除する
        /// </summary>
        void OnDestroy () 
        {
            Instance = null;
        }


        /// <summary>
        /// コンストラクタをprotectedにすることでインスタンスを生成出来なくする
        /// </summary>
        protected SingletonBase () {}


        /// <summary>
        /// Sceneの構築が完了したか
        /// </summary>
        /// <value><c>true</c> if system is done; otherwise, <c>false</c>.</value>
        public bool IsSetUpDone { get; protected set; }


        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
        }



    }

}
