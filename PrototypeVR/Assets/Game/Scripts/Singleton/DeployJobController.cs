﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Spawn;
using Game.Tag;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Singleton
{

    /// <summary>
    /// SingleTonBaseクラスのjobを実行するController
    /// </summary>
    public class DeployJobController : SingletonBase<DeployJobController>
    {

        /// <summary>
        /// 登録されるJOB
        /// </summary>
        [SerializeField]
        private List<SingletonBehaviour> jobList = new List<SingletonBehaviour>();


        /// <summary>
        /// 登録されたJOB
        /// </summary>
        public List<SingletonBehaviour> JobList 
        {
            get { return jobList; }
        }


        protected override void Awake ()
        {
            base.Awake ();
        }


        void Start ()
        {
            if (IsSetUpDone) 
            {
                return;
            }

            Observable
            .FromCoroutine (() => JobRoutine ())
            .Subscribe (_ => 
            {
                //
            });

            this.UpdateAsObservable ()
            .Where (_ => IsSetUpDone)
            .Take (1)
            .Subscribe (_ => 
            {
                SystemCreatedCheck();
            });
        }


        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public override void SystemCreatedCheck ()
        {
            JobList.ForEach (((SingletonBehaviour obj) => 
            {
                obj.SystemCreatedCheck();
            }));
        }


        /// <summary>
        /// タスクを実行する
        /// </summary>
        /// <returns>The routine.</returns>
        public override IEnumerator JobRoutine ()
        {
            if (Extension.IsNullOrEmpty<SingletonBehaviour> (JobList)) 
            {
                yield break;
            }

            List<IObservable<Unit>> streams = new List<IObservable<Unit>> ();

            foreach (SingletonBehaviour job in JobList) 
            {
                if (job == null) 
                {
                    continue;
                }
                streams.Add (Observable.FromCoroutine(() => job.JobRoutine()));
            }

            Observable.WhenAll (streams)
            .FirstOrDefault ()
            .Subscribe (_ => 
            {
                IsSetUpDone = true;
            });
            yield break;
        }

    }


}


