﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.VR;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.UI;
using Game.Interfaces.Item;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.StaticMesh;

using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;
using Util      = Game.Lib.GameComponentUtility;


namespace Game.FirstPerson
{

    /// <summary>
    /// FirstPersonの継承クラス
    /// </summary>
    public class FirstPersonController : FirstPersonBase
    {

        [SerializeField][Tooltip("移動可能な最小の首の傾き角度")]
        private float RotationAngularX = 15f;

        [SerializeField][Tooltip("移動不可能な最大の首の傾き角度")]
        private float LimitRotationAngulerX = 30f;

        /// <summary>
        /// Layer
        /// </summary>
        public int LayerMask { get { return CharacterActionController.LayerMask; } }


        /// <summary>
        /// 初期Y座標
        /// </summary>
        public float CameraInitOffsetY  { get; private set; }


        /// <summary>
        /// 首を下げたかどうか
        /// </summary>
        public bool IsNeckDown 
        {
            get 
            {
                if (CameraTransform == null) 
                {
                    return false;
                }

                float _angle = CameraTransform.eulerAngles.x;
                return (RotationAngularX < _angle && _angle < LimitRotationAngulerX);
            }
        }


        /// <summary>
        /// CameraのTransform
        /// </summary>
        public Transform CameraTransform 
        {
            get { return CameraFollowBase.RootTransform; }
        }


        /// <summary>
        /// PlayerActionControllerのWrapper
        /// </summary>
        public PlayerActionController PlayerCtrl 
        {
            get { return CharacterActionController as PlayerActionController; }
        }


        protected override void Awake ()
        {
            base.Awake ();
        }


        void Start ()
        {
            this.Initialize();

            // 首の傾きで動くevent
            this.UpdateAsObservable ()
            .Where (_ => CharacterActionController != null && 
                IsSetUpDone)
            .Subscribe (_ => 
            {
                PlayerCtrl.HandleMovement (IsNeckDown);
            })
            .AddTo (this);

            // 何かを捉えた時に発火するevent
            this.UpdateAsObservable ()
            .Where (_ => CameraFollowBase != null && 
                CameraFollowBase.LockTarget != null &&
                IsSetUpDone)
            .Subscribe (_ => 
            {
                PlayerCtrl.HandleGazenEvent(CameraFollowBase.LockTarget);
            })
            .AddTo (this);
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            base.Initialize ();
            base.CreateActionController ();
            base.CreateParameterModel ();
            base.CreateTroopController ();
            base.CreatePhysicsController ();


            Observable.FromCoroutine (() => Initializer ())
            .SelectMany (GetInteractiveContentsRoutine)
            .Subscribe (_ => 
            {
                ResultJobLog();
            });
        }


        /// <summary>
        /// 初期化JOB
        /// </summary>
        public override IEnumerator Initializer ()
        {
            IEnumerator routine = base.Initializer ();
            while (routine.MoveNext()) 
            {
                yield return null;
            }
            // Fpsカメラ取得
            if (CameraFollowBase == null) 
            {
                CameraFollowBase = RootTransform.GetComponentInChildren<CameraFollowBase> ();
                CameraFollowBase.Initialize (RayLength, LayerMask, GazeHoldTime);
            }
            // Offsetの取得
            {
                CameraInitOffsetY = RootTransform.position.y;
            }
            yield break;
        }


        /// <summary>
        /// 有効なコンテンツを取得する
        /// </summary>
        public override IEnumerator GetInteractiveContentsRoutine () 
        {
            // ユニークなtransformを取得する
            HashSet<Transform> hsTr = new HashSet<Transform> ();
            {
                foreach (var contents in FindObjectsOfType<ObjectBehaviour> ()) 
                {
                    // nullか自分が持つComponentはスキップする
                    if (contents == null || 
                        contents == this || 
                        contents == this.CharacterActionController ||
                        contents == this.CameraFollowBase) 
                    {
                        continue;
                    }
                    hsTr.Add (contents.RootTransform);
                }
            }

            // Baseクラスを取得する
            InteractiveContents = hsTr.Select ((Transform arg) => arg.GetComponent<ObjectBehaviour> ()).ToList ();
            hsTr.Clear ();
            yield break;
        }


        /// <summary>
        /// JOBが成功した時に呼ばれるlogメソッド
        /// </summary>
        protected override void ResultJobLog ()
        {
            if (Extension.IsNullOrEmpty<ObjectBehaviour> (InteractiveContents)) 
            {
                throw new System.Exception("Not Found VR InteractiveContents!!"); 
            }

            // Characterか静的MeshかUIかでわける
            // @TODO Singleton処理が完了する前に実行されるので良きタイミングで処理させる
            {
                List<CharacterBehaviour> characters    = new List<CharacterBehaviour> ();
                List<StaticObjectBehaviour> staticMesh = new List<StaticObjectBehaviour> ();
                List<UIObjectRootBehaviour> uiRoots    = new List<UIObjectRootBehaviour> ();
                List<ItemBehaviour> resourceList       = new List<ItemBehaviour> ();

                Util.InteractiveContentsByFilter (InteractiveContents, ref characters, ref staticMesh, ref uiRoots, ref resourceList);
                InteractiveCharacters = characters;
                StatisticContents     = staticMesh;
                UIContentsRoot        = uiRoots;
                InteractiveItemList   = resourceList;
            }

            // Componentが問題なく使えるか？
            {
                InteractiveContents.Clear ();
                IsSetUpDone = 
                    !Extension.IsNullOrEmpty<CharacterBehaviour>(InteractiveCharacters) ||
                    !Extension.IsNullOrEmpty<StaticObjectBehaviour>(StatisticContents) ||
                    !Extension.IsNullOrEmpty<UIObjectRootBehaviour>(UIContentsRoot) ||
                    !Extension.IsNullOrEmpty<ItemBehaviour>(InteractiveItemList);
            }
        }



    }


}

