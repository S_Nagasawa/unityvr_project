﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Example
{

    public class TempCubeScript : StaticObjectBehaviour
    {
        [SerializeField]
        private Rigidbody _Body;

        [SerializeField]
        private float _DefaultPower = 30f;



        protected override void Awake ()
        {
            base.Awake ();
            _Body = Extension.TryComponent<Rigidbody> (RootTransform.gameObject);
        }



        public void Play (ObjectBehaviour arg)
        {
            if (arg == null) 
            {
                return;
            }

            CharacterBehaviour character = arg as CharacterBehaviour;
            if (character == null) 
            {
                return;
            }
            var forward = transform.TransformDirection (arg.RootTransform.forward);
            var power   = (character.CharacterParameterModel != null) ? character.CharacterParameterModel.Power : _DefaultPower;
            _Body.AddForce (forward * power, ForceMode.Impulse);
        }

    }


}

