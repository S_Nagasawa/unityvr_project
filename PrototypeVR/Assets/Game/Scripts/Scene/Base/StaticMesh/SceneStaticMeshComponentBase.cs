﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Scene.Base.StaticMesh
{
    /// <summary>
    /// 各シーンに依存するStaticMeshクラスの基底クラス
    /// </summary>
    public class SceneStaticMeshComponentBase : CoreBehaviour
    {
    }


}
