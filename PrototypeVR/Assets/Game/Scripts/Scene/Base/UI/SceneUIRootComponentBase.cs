﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Scene.Base.UI
{

    /// <summary>
    /// 各シーンに依存するUI Canvasクラスの基底クラス
    /// </summary>
    public class SceneUIRootComponentBase : CoreBehaviour
    {

        /// <summary>
        /// Canvas
        /// </summary>
        public virtual UIObjectRootBehaviour UIRootBehaviour { get; protected set; }

        /// <summary>
        /// 単一のUIComponent
        /// </summary>
        public virtual UIObjectBehaviour SelectedUIItemComponent { get; protected set; }


        /// <summary>
        /// 複数のUIComponent
        /// </summary>
        public virtual List<UIObjectBehaviour> UIItemComponentList { get; protected set; }


        /// <summary>
        /// 複数あるUIの内、1つでもHoldされているか？
        /// </summary>
        public virtual bool IsHoldAny ()
        {
            return false;
        }

        /// <summary>
        /// 複数あるUIの内、全てHoldされているか？
        /// </summary>
        public virtual bool IsHoldAll ()
        {
            return false;
        }


        /// <summary>
        /// 複数あるUIの内、1つでも視線が入ったかどうか？
        /// </summary>
        public virtual bool IsPointerEntryAny () 
        {
            return false;
        }


        /// <summary>
        /// Actionを開始する
        /// </summary>
        public virtual void Play () 
        {
            IsPlaying = true;
        }


        /// <summary>
        /// Actionを停止する
        /// </summary>
        public virtual void Stop () 
        {
            IsPlaying = false;
        }


        /// <summary>
        /// 再開する
        /// </summary>
        public virtual void Resume () {}


        /// <summary>
        /// 再生中か？
        /// </summary>
        public virtual bool IsPlaying { get; protected set; }


        /// <summary>
        /// 強制的に停止する
        /// </summary>
        public virtual void Execute () {}


        /// <summary>
        /// 強制的に実行する
        /// </summary>
        public virtual void Invoker () {}
    }


}

