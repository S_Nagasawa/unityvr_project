﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.Scene;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Scene.Base.UI
{

    /// <summary>
    /// 各シーンに依存するUI Partsクラスの基底クラス
    /// </summary>
    public class SceneUIItemComponentBase : CoreBehaviour
    {

        /// <summary>
        /// Occurs when on play action.
        /// </summary>
        public virtual event System.Action<string> OnSceneAction = delegate {};


        [SerializeField][Tooltip("Sceneを切り替えるか？")]
        protected bool _IsChangeScene;

        public bool IsChangeScene
        {
            get { return _IsChangeScene; }
        }

        [SerializeField][Tooltip("切り替えるScene名")]
        protected string _SceneName;

        public string SceneName
        {
            get { return _SceneName; }
        }

        /// <summary>
        /// UIComponentの基底クラス
        /// </summary>
        public UIObjectBehaviour UIItemComponent { get; protected set; }


        /// <summary>
        /// アクションによってScene切り替えが発生するかどうか？
        /// </summary>
        public virtual SceneParamBase SceneParam { get; protected set; }


        /// <summary>
        /// Actionを開始する
        /// </summary>
        public virtual void PlayAction () {}


        /// <summary>
        /// Actionを停止する
        /// </summary>
        public virtual void StopAction () {}


        /// <summary>
        /// Updates the scene parameter.
        /// </summary>
        public void UpdateSceneParam (SceneParamBase sceneParam) 
        {
            SceneParam = sceneParam;
        }

    }


}

