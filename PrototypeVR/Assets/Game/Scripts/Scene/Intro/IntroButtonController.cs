﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using Game.Scene.Base.UI;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Scene.Intro
{

    /// <summary>
    /// IntroSceneで使うButtonイベントController
    /// </summary>
    public class IntroButtonController : SceneUIItemComponentBase 
    {
        [SerializeField]
        private UIObjectBehaviour _UIItem;


        /// <summary>
        /// Occurs when on play action.
        /// </summary>
        public override event System.Action<string> OnSceneAction = delegate {};


        protected override void Awake ()
        {
            base.Awake ();
        }


        void Start ()
        {
            Initialize ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            UIItemComponent = GetComponent<UIObjectBehaviour> ();

            // dummy
            {
                _UIItem = UIItemComponent;
            }
        }


        /// <summary>
        /// Actionを開始する
        /// </summary>
        public override void PlayAction ()
        {
            base.PlayAction ();
            if (_IsChangeScene) 
            {
                OnSceneAction (_SceneName);
            }
        }


    }


}

