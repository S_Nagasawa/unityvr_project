﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Interfaces.Scene;
using Game.Spawn;
using Game.Tag;
using Game.Singleton;

using UniRx;
using UniRx.Triggers;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Scene.Intro
{


    /// <summary>
    /// IntroSceneで使うパラメーター各種
    /// SceneParamBaseの派生クラス
    /// </summary>
    public class IntroSceneParam : SceneParamBase
    {
        

        public IntroSceneParam() : base () 
        {
            
        }

        public IntroSceneParam(string sceneName)
        {
            SceneName = sceneName;
        }
    }


    public class IntroSceneParamBuilder 
    {
        public IntroSceneParam Create ()
        {
            return new IntroSceneParam ();
        }

        public IntroSceneParam Create (string sceneName)
        {
            return new IntroSceneParam (sceneName);
        }
    }
}

