﻿using UnityEngine;
using UnityEngine.SceneManagement; 
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Spawn;
using Game.Tag;
using Game.Singleton;
using Game.Scene.Base.UI;
using Game.Scene.Base.Actor;
using Game.Scene.Base.StaticMesh;
using Game.Enums;

using UniRx;
using UniRx.Triggers;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Scene.Intro
{

    /// <summary>
    /// IntroSceneで使うSingletonController
    /// </summary>
    public class IntroSceneController : SingletonBase<IntroSceneController>
    {
        /// <summary>
        /// ボタンが押された親
        /// </summary>
        [SerializeField]
        private List<UIObjectRootBehaviour> _Roots = new List<UIObjectRootBehaviour>();

        /// <summary>
        /// ボタン自身
        /// </summary>
        [SerializeField]
        private List<UIObjectBehaviour> _InItems = new List<UIObjectBehaviour> ();


        protected override void Awake ()
        {
            base.Awake ();
        }


        void Start ()
        {
            if (IsSetUpDone) 
            {
                return;
            }
            Observable
            .FromCoroutine (() => JobRoutine ())
            .Subscribe (_ => 
            {
                //
            });

            this.UpdateAsObservable ()
            .Where (_ => IsSetUpDone)
            .Take (1)
            .Subscribe (_ => 
            {
                SystemCreatedCheck();
            });
        }
    

        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        public override IEnumerator JobRoutine ()
        {
            SceneUIRootComponentBase[] uiRoot = FindObjectsOfType<SceneUIRootComponentBase> ().ToArray ();
            HashSet<UIObjectBehaviour> items  = new HashSet<UIObjectBehaviour> ();

            foreach (SceneUIRootComponentBase parts in uiRoot) 
            {
                if (parts == null)
                {
                    continue;
                }

                foreach (UIObjectBehaviour obj in Extension.GetComponentsInChildrenWithoutSelf<UIObjectBehaviour>(parts.RootTransform)) 
                {
                    if (obj == null) 
                    {
                        continue;
                    }
                    items.Add (obj);
                }
                yield return null;
            }

            // ボタンタイプのみListに追加
            {
                _InItems = items.Where ((UIObjectBehaviour arg) => arg.UIType == UIType.BUTTON).ToList ();
            }

            // rootの追加
            {
                foreach (UIObjectBehaviour arg in _InItems) 
                {                    
                    if (arg == null || _Roots.Contains (arg.RootCanvas)) 
                    {
                        continue;
                    }
                    _Roots.Add (arg.RootCanvas);
                }
            }

            // Scene切り替えの場合デリゲードの追加
            {
                foreach (UIObjectBehaviour arg in _InItems) 
                {                    
                    if (arg == null) 
                    {
                        continue;
                    }
                    SceneUIItemComponentBase comp = arg.GetComponent<SceneUIItemComponentBase> ();
                    if (comp != null &&
                        comp.IsChangeScene) 
                    {
                        comp.OnSceneAction += SceneChangeAction;
                    }
                }
            }


            items.Clear ();
            IsSetUpDone = true;
            yield break;
        }


        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public override void SystemCreatedCheck ()
        {
            IsSetUpDone = !Extension.IsNullOrEmpty<UIObjectBehaviour> (_InItems);

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (!IsSetUpDone || Instance == null) 
            {
                Debug.Log(string.Format("<color=red> {0}_FailJob </color>", Instance));
            }
            else
            {
                Debug.Log(string.Format("<color=cyan> {0}_SuccessJob </color>", Instance));                          
            }
            #endif
        }


        /// <summary>
        /// Scene切り替えボタンがholdされた時に呼ばれる
        /// </summary>
        private void SceneChangeAction (string sceneName)
        {
            if (Instance == null) 
            {
                return;
            }

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.Log (string.Format("<color=blue> SceneChangeAction_{0} </color>", Instance));
            #endif

            string currentSceneName = SceneManager.GetActiveScene ().name;

            if (sceneName == currentSceneName) 
            {
                Debug.Log (string.Format("<color=red> Already CurrentScene!! __{0} </color>", currentSceneName));
                return;
            }
            SceneFadeController.Instance.LoadLevel (sceneName, null);
        }


    }


}
