﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using Game.Scene.Base.UI;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Scene.Intro
{

    /// <summary>
    /// IntroシーンのMenuボタンを管理するController
    /// </summary>
    public class MenuButtonRootController : SceneUIRootComponentBase
    {
        [SerializeField] 
        private List<UIObjectBehaviour> _UIItemComponentList = new List<UIObjectBehaviour>();


        /// <summary>
        /// パラメーター付きComponent基底クラス
        /// </summary>
        public SceneUIItemComponentBase _ParamItemComponent 
        {
            get { return SelectedUIItemComponent.GetComponent<SceneUIItemComponentBase> (); }
        }


        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
            Initialize ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            UIRootBehaviour     = RootTransform.GetComponent<UIObjectRootBehaviour> ();
            UIItemComponentList = Extension.GetComponentsInChildrenWithoutSelf<UIObjectBehaviour> (gameObject).ToList ();

            // dummy
            {
                _UIItemComponentList = UIItemComponentList;
            }
        }


        void Start ()
        {
            // 子供のUIItemを監視する
            this.UpdateAsObservable ()
            .ObserveEveryValueChanged (x => IsHoldAny())
            .Where (x => x)
            .Subscribe (_ => 
            {
                Invoker();
            })
            .AddTo (this);
        }


        /// <summary>
        /// 複数あるUIの内、1つでもHoldされているか？
        /// </summary>
        public override bool IsHoldAny ()
        {
            // Emptyならfalse
            if (Extension.IsNullOrEmpty<UIObjectBehaviour> (UIItemComponentList)) 
            {
                return false;
            }

            // 視線に入ったUIを取得する
            {
                SelectedUIItemComponent = UIItemComponentList
                    .Where ((UIObjectBehaviour arg) => arg.IsLock)
                    .FirstOrDefault();
            }

            // Gazenイベントをcall
            bool isGazen = UIRootBehaviour.CanGazenUIEvent ();

            // そのUIがHoldされるか？
            return (isGazen && SelectedUIItemComponent != null && SelectedUIItemComponent.IsHold);
        }


        /// <summary>
        /// 強制的に実行する
        /// </summary>
        public override void Invoker ()
        {
            if (SelectedUIItemComponent == null) 
            {
                return;
            }
            SelectedUIItemComponent.OnUIAction();

            if (_ParamItemComponent == null) 
            {
                return;
            }
            _ParamItemComponent.PlayAction ();
        }



    }


}
