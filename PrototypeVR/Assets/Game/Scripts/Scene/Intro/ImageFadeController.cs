﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using Game.Scene.Base.UI;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Scene.Intro
{

    /// <summary>
    /// CanvasにつけるfadeController
    /// 子供のImageComponentをFadeIn/Outをする
    /// </summary>
    public class ImageFadeController : SceneUIRootComponentBase
    {

        /// <summary>
        /// UIObjectBehaviourを持っているImageComponent
        /// </summary>
        [SerializeField]
        private List<Sprite> _SpriteList = new List<Sprite>();

        [SerializeField]
        private Image _UIImage;

        [SerializeField]
        private float _Interval = 3f;

        [SerializeField]
        private int _Index;

        private Quaternion _InitialQue;

        public int Index
        {
            get { return _Index; }
        }



        protected override void Awake ()
        {
            base.Awake ();
        }


        void Start ()
        {
            Initialize ();

            // 一定の距離にCharacterがいた場合にApplyRotationが実行される
            this.UpdateAsObservable ()
            .Where(_ => UIRootBehaviour != null && UIRootBehaviour.CanGazenUIEvent())
            .Subscribe (_ => 
            {
                UIRootBehaviour.ApplyTurnRotation();
            })
            .AddTo (this);            

            Observable.EveryUpdate ()
            .Subscribe (_ => 
            {
                Resume();
            })
            .AddTo (this);
        }


        /// <summary>
        /// 再開する
        /// </summary>
        public override void Resume ()
        {
            if (SelectedUIItemComponent.IsHold) 
            {
                Play ();
            }
            else
            {
                Stop ();
            }
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            UIRootBehaviour         = RootTransform.GetComponent<UIObjectRootBehaviour> ();
            SelectedUIItemComponent = Extension.GetComponentsInChildrenWithoutSelf<UIObjectBehaviour> (gameObject).FirstOrDefault();
            _UIImage    = SelectedUIItemComponent.GetComponent<Image> ();
            _InitialQue = RootTransform.rotation;
            _Index      = 0;
            _SpriteList.Add (_UIImage.sprite);
            _SpriteList.Reverse ();
        }


        /// <summary>
        /// Spriteを変えるCoroutine
        /// </summary>
        /// <returns>The sprite routine.</returns>
        /// <param name="callback">Callback.</param>
        private IEnumerator ChangeSpriteRoutine ()
        {

            while (SelectedUIItemComponent.IsHold) 
            {
                {
                    OnNextIndex ();
                    yield return StartCoroutine (FadeOutEvent());
                }

                {
                    _UIImage.sprite = _SpriteList [_Index];
                    yield return null;
                }

                {
                    yield return StartCoroutine (FadeInEvent());
                }
            }
            yield break;
        }



        private IEnumerator FadeOutEvent ()
        {
            float _time = _Interval;
            while (_time > 0.0f) 
            {
                _time -= Time.deltaTime;
                _UIImage.color = new Color (255f, 255f, 255f, _time);
                yield return null;
            }
            yield break;
        }



        private IEnumerator FadeInEvent ()
        {
            float _time = 0.0f;
            while (_time < _Interval) 
            {
                _time += Time.deltaTime;
                _UIImage.color = new Color (255f, 255f, 255f, _time);
                yield return null;
            }
            yield break;
        }


        /// <summary>
        /// 次のIndexを選択する
        /// </summary>
        private void OnNextIndex ()
        {
            if ((_SpriteList.Count - 1) <= _Index) 
            {
                _Index = 0;
            }
            else
            {
                _Index++;
            }
        }


        /// <summary>
        /// Actionを開始する
        /// </summary>
        public override void Play ()
        {
            if (IsPlaying) 
            {
                return;
            }
            base.Play ();
            Observable.FromCoroutine (() => ChangeSpriteRoutine ())
            .Subscribe (_ => 
            {
                // Loopを抜けた際に呼ばれる
            });
        }


        /// <summary>
        /// Actionを停止する
        /// </summary>
        public override void Stop ()
        {
            if (!IsPlaying) 
            {
                return;
            }
            base.Stop ();
            _UIImage.sprite = _SpriteList.FirstOrDefault ();
            _Index = 0;
            Observable.FromCoroutine (() => UIRootBehaviour.InitialTurnRotation(_InitialQue, _Interval))
            .Subscribe (_ => 
            {
                // 
            });
        }



    }


}

