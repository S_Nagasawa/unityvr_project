﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.StaticMesh
{

    /// <summary>
    /// 静的なObjectの継承クラス
    /// </summary>
    [RequireComponent(typeof(EventTriggerController))]
    public class StaticMeshComponent : StaticObjectBehaviour
    {
        [SerializeField]
        private string _Name;

        [SerializeField]
        private Color _ChangeColor;

        private Color _DefaultColor;

        /// <summary>
        /// すでにLockされているか？
        /// </summary>
        private bool isAlreadyLock;


        protected override void Awake ()
        {
            base.Awake ();
            Renderer      = GetComponent<Renderer> () as MeshRenderer;
            _DefaultColor = Renderer.material.color;
            _Name         = StaticObjectModel.Name;
        }



        void Start ()
        {
            this.UpdateAsObservable ()
            .Where(_ => isAlreadyLock != IsLock)
            .Subscribe (_ => 
            {
                SetMaterial();
            })
            .AddTo (this);
        }


        private void SetMaterial ()
        {
            if (Renderer == null) 
            {
                return;
            }
            Renderer.material.color = IsLock ? _ChangeColor : _DefaultColor;
            isAlreadyLock = IsLock;
        }


        /// <summary>
        /// PointerがHoldされている時に呼ばれる *Player引数付き
        /// </summary>
        /// <param name="arg">Argument.</param>
        public override void ApplyGazenPointerHold (ObjectBehaviour arg)
        {
            if (IsApplyGazenEventHold) 
            {
                return;
            }
            base.ApplyGazenPointerHold ();
        }
    }


}

