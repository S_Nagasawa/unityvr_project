﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Interfaces;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Events;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Weapon;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces
{

    /// <summary>
    /// Characterの基底クラス
    /// 人以外も含む
    /// </summary>
    public abstract class CharacterBehaviour : ObjectBehaviour, IActionMethod, IActionTasks, IGameComponents, ICharacterObjectModel
    {

        /// <summary>
        /// AISystemの辞書
        /// Key AIType
        /// Value Object
        /// </summary>
        public Dictionary<AISystemType, ObjectBehaviour> AITypeDict { get; protected set; }


        /// <summary>
        /// Parameterを管理するinterface
        /// </summary>
        public ICharacterParameter CharacterParameterModel { get; protected set; }


        /// <summary>
        /// 基本動作を管理するクラス
        /// </summary>
        public CharacterActionControllerBase CharacterActionController { get; protected set; }


        /// <summary>
        /// 武器を管理するクラス
        /// </summary>
        public WeaponControllerBase WeaponController { get; protected set; }


        /// <summary>
        /// TroopController
        /// </summary>
        public TroopBase TroopController { get; protected set; }


        /// <summary>
        /// 当たり判定を管理するController
        /// </summary>
        public PhysicsControllerBase PhysicsController { get; protected set; }


        /// <summary>
        /// AIシステムリスト
        /// </summary>
        public List<AISystemBase> AISystemList { get; protected set; }


        /// <summary>
        /// 見つけたターゲット
        /// </summary>
        public ObjectBehaviour LockTarget { get; protected set; }


        /// <summary>
        /// 歩兵種別をEnumで取得する
        /// </summary>
        public InfantryType CharacterInfantryType 
        {
            get { return CharacterParameterModel.CharacterInfantryType; }
        }


        /// <summary>
        /// Playerが見ているか？
        /// </summary>
        public bool IsLock
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsLock;
            }
        }


        /// <summary>
        /// Playerが一定時間見ているか？
        /// </summary>
        public bool IsHold
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsHold;
            }
        }


        /// <summary>
        /// 全てのAIシステムが有効か？
        /// </summary>
        public bool IsAISystemAvailableAll
        {
            get { return AISystemList.All ((AISystemBase arg) => arg.IsValidSystem); }
        }


        #region SerializeField
        [SerializeField][Tooltip("WorldUIが検知するまでの距離")]
        protected float _UIDistance = 5f;
        public float UIDistance { get { return _UIDistance; } }


        [SerializeField][Tooltip("TroopControllerに渡す基本的な距離")]
        protected float _RemainingDistance = 12f;
        public float RemainingDistance { get { return _RemainingDistance; } }

        [SerializeField][Tooltip("SquadType")]
        protected SquadType _SquadType;
        public SquadType SquadType { get { return _SquadType; } }
        #endregion


        /// <summary>
        /// LifeCycleの実行、死亡したら1度のみ呼ばれる
        /// </summary>
        protected IObservable<long> LifeCycleStream = null;


        #region SetUp Component
        /// <summary>
        /// CharacterParameterModelのinstanceを生成する
        /// </summary>
        public virtual void CreateParameterModel ()
        {            
            if (CharacterParameterModel == null) 
            {
                #if UNITY_EDITOR || DEVELOPMENT_BUILD
                CharacterParameterModel = new CharacterParameterModelFactory ().CreateDummy ();
                #elif
                CharacterParameterModel = new CharacterParameterModelFactory ().Create ();
                #endif
            }
        }


        /// <summary>
        /// ActionControllerのセットアップ
        /// </summary>
        public virtual void CreateActionController ()
        {
            if (CharacterActionController == null) 
            {
                CharacterActionController = RootTransform.GetComponent<CharacterActionControllerBase> ();
            }
            CharacterActionController.Initialize (this);
        }


        /// <summary>
        /// ActionController + Agentのセットアップ
        /// </summary>
        public virtual void CreateActionControllerAgentSetUp ()
        {
            if (CharacterActionController == null) 
            {
                CharacterActionController = RootTransform.GetComponent<CharacterActionControllerBase> ();
            }
            CharacterActionController.Initialize (this);
            CharacterActionController.SetUpAgent ();
        }


        /// <summary>
        /// AICharacter向け EventTriggerCtrlを初期化する
        /// </summary>
        public virtual void CreateEventTrigger ()
        {
            if (TriggerController == null) 
            {
                TriggerController = RootTransform.GetComponent<EventTriggerBase> ();
            }
            TriggerController.Initialize ();
        }


        /// <summary>
        /// WeaponControllerのセットアップ
        /// </summary>
        public virtual void CreateWeaponController ()
        {
            if (WeaponController == null) 
            {
                WeaponController = RootTransform.GetComponent<WeaponControllerBase> ();
            }
            WeaponController.Initialize (this);
        }


        /// <summary>
        /// TroopControllerを初期化する
        /// </summary>
        public virtual void CreateTroopController (List<CharacterBehaviour> actors)
        {
            if (TroopController == null) 
            {
                TroopController = new TroopControllerFactory ().Create (this, actors, RemainingDistance);
            }
            TroopController.Initialize ();
        }


        /// <summary>
        /// TroopControllerを初期化する
        /// </summary>
        public virtual void CreateTroopController()
        {
            if (TroopController == null) 
            {
                TroopController = new TroopControllerFactory ().Create (this, RemainingDistance);
            }
            TroopController.Initialize ();
        }


        /// <summary>
        /// PhysicsControllerを初期化する
        /// </summary>
        public virtual void CreatePhysicsController ()
        {
            if (PhysicsController == null) 
            {
                PhysicsController = RootTransform.GetComponent<PhysicsControllerBase> ();
            }
            PhysicsController.Initialize (this);
        }
        #endregion


        /// <summary>
        /// 生存しているか？
        /// </summary>
        public virtual bool IsAlive ()
        {
            return CharacterParameterModel.IsAlive ();
        }


        /// <summary>
        /// 待ち
        /// </summary>
        public virtual void Idle () 
        {
            CharacterActionController.StopDestinationHandler ();
        }


        /// <summary>
        /// 歩く
        /// </summary>
        public virtual void Walk () 
        {
            CharacterActionController.SetDestinationHandler ();
        }


        /// <summary>
        /// 走る
        /// </summary>
        public virtual void Run () {}


        /// <summary>
        /// 飛ぶ
        /// </summary>
        public virtual void Jump () {}


        /// <summary>
        /// 物をとる
        /// </summary>
        public virtual void Pick () {}


        /// <summary>
        /// 構える
        /// </summary>
        public virtual void Aim () 
        {            
            CharacterActionController.AimDestinationHandler ();
            WeaponController.Shot ();
        }


        /// <summary>
        /// ターゲットの更新
        /// </summary>
        public virtual void SetLockTarget (ObjectBehaviour obj)
        {
            LockTarget = obj;
        }


        /// <summary>
        /// 目的が完了したら呼ばれる
        /// </summary>
        protected virtual void TaskAction () 
        {
            if (LockTarget == null) 
            {
                return;
            }
            if (LockTarget as SpawnObjectBase) 
            {
                (LockTarget as SpawnObjectBase).UpdatePassed (true);                
            }
        }


        /// <summary>
        /// 次のタスクの処理をする
        /// </summary>
        public virtual void ThinkNextTask () {}


        /// <summary>
        /// 前のタスクの処理をする
        /// </summary>
        public virtual void ThinkPreviousTask () {}


        /// <summary>
        /// 全てのタスクを完了させる
        /// </summary>
        public virtual void AllTaskDone () {}


        /// <summary>
        /// 全てのタスクをリセットする
        /// </summary>
        public virtual void ResetTaskDone () {}


        /// <summary>
        /// タスクの接続
        /// </summary>
        public virtual void ConnectTask ()
        {
            CharacterActionController.ConnectTask ();
        }


        /// <summary>
        /// 各種デリゲードのセット
        /// </summary>
        protected virtual void AddDelegate ()
        {
            CharacterActionController.OnTaskAction += TaskAction;
        }


        /// <summary>
        /// 各種デリゲードのクリア
        /// </summary>
        protected virtual void RemoveDelegate ()
        {
            CharacterActionController.OnTaskAction -= TaskAction;
        }


        /// <summary>
        /// JOBが成功した時に呼ばれるlogメソッド
        /// </summary>
        protected virtual void ResultJobLog () {}


        /// <summary>
        /// AISystem種別からAISystemを返す
        /// </summary>
        /// <returns>The AI system of type.</returns>
        /// <param name="systemType">System type.</param>
        public virtual AISystemBase GetAISystemOfType (AISystemType systemType)
        {
            AISystemBase system = AISystemList.Where (((AISystemBase arg) => arg.AISystemType == systemType))
                .FirstOrDefault ();

            if (system.IsValidSystem) 
            {
                return system;
            }
            return null;
        }


        /// <summary>
        /// Damageを与える
        /// </summary>
        public virtual void ApplyTakenDamage ()
        {            
        }


        /// <summary>
        /// Damageを与える
        /// </summary>
        public virtual void ApplyTakenDamage (int damageAmount)
        {            
        }


    }

}


