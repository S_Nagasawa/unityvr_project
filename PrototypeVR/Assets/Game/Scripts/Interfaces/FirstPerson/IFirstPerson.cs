﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.UI;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.StaticMesh;
using Game.UI;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.FirstPerson
{

    /// <summary>
    /// FirstPersonのinterface
    /// </summary>
    public interface IFirstPerson
    {
        /// <summary>
        /// VR空間上に存在する有効なコンテンツ
        /// </summary>
        List<ObjectBehaviour> InteractiveContents { get; }

        /// <summary>
        /// VR空間上に存在する有効なCharacter
        /// </summary>
        List<CharacterBehaviour> InteractiveCharacters { get; }

        /// <summary>
        /// VR空間上に存在する静的なコンテンツ
        /// </summary>
        List<StaticObjectBehaviour> StatisticContents { get; }

        /// <summary>
        /// VR空間上に存在するUIコンテンツ *Canvas
        /// </summary>
        List<UIObjectRootBehaviour> UIContentsRoot { get; }

        /// <summary>
        /// VR空間上に存在するResource
        /// </summary>
        List<ItemBehaviour> InteractiveItemList { get; }

        /// <summary>
        /// VR空間上に存在する有効なコンテンツを取得するJOB
        /// </summary>
        IEnumerator GetInteractiveContentsRoutine ();

        /// <summary>
        /// 一人称カメラ
        /// </summary>
        CameraFollowBase CameraFollowBase { get; }
    }


}
