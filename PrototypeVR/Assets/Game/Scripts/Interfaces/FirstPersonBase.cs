﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.UI;
using Game.Interfaces.FirstPerson;
using Game.Interfaces.Item;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.StaticMesh;
using Game.UI;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces
{


    /// <summary>
    /// FirstPersonの基底クラス
    /// </summary>
    public abstract class FirstPersonBase : CharacterBehaviour, IFirstPerson
    {

        [SerializeField][Tooltip("Holdイベントが発生するまでの時間")]
        public float GazeHoldTime = 2.4f;

        [SerializeField][Tooltip("Rayの長さ")]
        public float RayLength = 10f;

        /// <summary>
        /// VR空間上に存在する有効なコンテンツ
        /// </summary>
        public List<ObjectBehaviour> InteractiveContents { get; protected set; }

        /// <summary>
        /// VR空間上に存在する有効なCharacter
        /// </summary>
        public List<CharacterBehaviour> InteractiveCharacters { get; protected set; }

        /// <summary>
        /// VR空間上に存在する静的なコンテンツ
        /// </summary>
        public List<StaticObjectBehaviour> StatisticContents { get; protected set; }

        /// <summary>
        /// VR空間上に存在するUIコンテンツ *Canvas
        /// </summary>
        public List<UIObjectRootBehaviour> UIContentsRoot { get; protected set; } 

        /// <summary>
        /// VR空間上に存在するResource
        /// </summary>
        public List<ItemBehaviour> InteractiveItemList { get; protected set; }

        /// <summary>
        /// 一人称カメラ
        /// </summary>
        public CameraFollowBase CameraFollowBase { get; protected set; }

        /// <summary>
        /// VR空間上に存在する有効なコンテンツを取得するJOB
        /// </summary>
        public virtual IEnumerator GetInteractiveContentsRoutine () 
        {
            yield break;
        }

    }



}

