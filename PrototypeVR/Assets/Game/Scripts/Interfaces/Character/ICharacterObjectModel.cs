﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces.Character
{

    /// <summary>
    /// CharacterModelが保持するinterface
    /// </summary>
    public interface ICharacterObjectModel
    {
        /// <summary>
        /// Parameterを管理するinterface
        /// </summary>
        ICharacterParameter CharacterParameterModel { get; }

        /// <summary>
        /// CharacterParameterModelのinstanceを生成する
        /// </summary>
        void CreateParameterModel();
    }


}


