﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using Game.Enums;

namespace Game.Interfaces.Character
{

    /// <summary>
    /// Parameterを管理するinterface
    /// </summary>
    public interface ICharacterParameter
    {

        /// <summary>
        /// 名前
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 体力
        /// </summary>
        int Health { get; }

        /// <summary>
        /// 攻撃
        /// </summary>
        uint Attack { get; }

        /// <summary>
        /// 守備
        /// </summary>
        uint Defence { get; }

        /// <summary>
        /// 知力
        /// </summary>
        uint Wisdom { get; }

        /// <summary>
        /// 速度
        /// </summary>
        float Speed { get; }

        /// <summary>
        /// 力
        /// </summary>
        float Power { get; }

        /// <summary>
        /// 歩兵種別
        /// </summary>
        byte Infantry { get; }

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize ();

        /// <summary>
        /// 生存しているか
        /// </summary>
        bool IsAlive();

        /// <summary>
        /// 歩兵種別をEnumで取得する
        /// </summary>
        InfantryType CharacterInfantryType { get; }

        /// <summary>
        /// ダメージを与える
        /// </summary>
        void ApplyTakenDamage (int damageAmount);
    }


    /// <summary>
    /// Character model base.
    /// </summary>
    public abstract class CharacterParameterModelBase : ICharacterParameter
    {
        /// <summary>
        /// SampleData
        /// </summary>
        public static Dictionary<string, object> InitData = new Dictionary<string, object> () 
        {
            {"Name", "Ethan"},
            {"Health", 1000},
            {"Attack", 100},
            {"Defence", 100},
            {"Wisdom", 100},
            {"Speed", 4},
            {"Power", 30},
            {"Infantry", 1}
        };

        /// <summary>
        /// 名前
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; protected set; }

        /// <summary>
        /// 体力
        /// </summary>
        /// <value>The health.</value>
        public int Health { get; protected set; }

        /// <summary>
        /// 攻撃
        /// </summary>
        /// <value>The attack.</value>
        public uint Attack { get; protected set; }

        /// <summary>
        /// 守備
        /// </summary>
        /// <value>The defence.</value>
        public uint Defence { get; protected set; }

        /// <summary>
        /// 知力
        /// </summary>
        /// <value>The wisdom.</value>
        public uint Wisdom { get; protected set; }

        /// <summary>
        /// 速度
        /// </summary>
        /// <value>The speed.</value>
        public float Speed { get; protected set; }

        /// <summary>
        /// 力
        /// </summary>
        /// <value>The power.</value>
        public float Power { get; protected set; }

        /// <summary>
        /// 歩兵種別
        /// </summary>
        /// <value>The infantry.</value>
        public byte Infantry { get; protected set; }

        /// <summary>
        /// EnumでInfantryを取得
        /// </summary>
        public InfantryType CharacterInfantryType
        {
            get { return (InfantryType)Infantry; }
        }

        /// <summary>
        /// 生存しているか
        /// </summary>
        public bool IsAlive()
        {
            return Health > 0;
        }

        public virtual void Initialize() 
        {

        }

        /// <summary>
        /// ダメージを与える
        /// </summary>
        /// <param name="damageAmount">Damage amount.</param>
        public void ApplyTakenDamage (int damageAmount)
        {
            int health = Health - damageAmount;

            // すでに死んでいるか？
            if (!IsAlive ()) 
            {
                return;
            }
                
            // オーバーキルか？
            if (health < 0) 
            {
                health = 0;
            }
            Health = health;
        }


        public override string ToString ()
        {
            return string.Format ("[CharacterParameterModelBase: " +
                "Name={0}, " +
                "Health={1}, " +
                "Attack={2}, " +
                "Defence={3}, " +
                "Wisdom={4}, " +
                "Speed={5}" +
                "Power={6}" +
                "Infantry={7}" +
                "CharacterInfantryType={8}]", 
                Name, 
                Health, 
                Attack, 
                Defence, 
                Wisdom, 
                Speed, 
                Power, 
                Infantry, 
                CharacterInfantryType);
        }
    }


    /// <summary>
    /// Character parameter model.
    /// </summary>
    public class CharacterParameterModel : CharacterParameterModelBase 
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Game.Interfaces.CharacterParameterModel"/> class.
        /// </summary>
        public CharacterParameterModel () : base ()
        {

        }


        public CharacterParameterModel (IDictionary dictionary) : base () 
        {
            if (dictionary.Contains ("Name")) 
            {
                Name = dictionary ["Name"].ToString ();
            }

            if (dictionary.Contains ("Attack")) 
            {
                Attack = uint.Parse (dictionary ["Attack"].ToString ());
            }

            if (dictionary.Contains ("Health")) 
            {
                Health = int.Parse (dictionary ["Health"].ToString ());
            }

            if (dictionary.Contains ("Defence")) 
            {
                Defence = uint.Parse (dictionary ["Defence"].ToString ());
            }

            if (dictionary.Contains ("Wisdom")) 
            {
                Wisdom = uint.Parse (dictionary ["Wisdom"].ToString ());
            }

            if (dictionary.Contains ("Speed")) 
            {
                Speed = float.Parse (dictionary ["Speed"].ToString ());
            }

            if (dictionary.Contains ("Power")) 
            {
                Power = float.Parse (dictionary ["Power"].ToString ());
            }

            if (dictionary.Contains ("Infantry")) 
            {
                #if false
                Infantry = (byte)Random.Range(1, 3);
                #endif
                Infantry = byte.Parse (dictionary ["Infantry"].ToString ());

            }
        }

        public override void Initialize()
        {
            base.Initialize ();
        }

        public void Clone (ICharacterParameter characterParams)
        {
            if (characterParams == null) 
            {
                return;
            }
            Name     = characterParams.Name;
            Attack   = characterParams.Attack;
            Health   = characterParams.Health;
            Defence  = characterParams.Defence;
            Wisdom   = characterParams.Wisdom;
            Speed    = characterParams.Speed;
            Power    = characterParams.Power;
            Infantry = characterParams.Infantry;
        }
    }


    /// <summary>
    /// Character parameter model factory.
    /// </summary>
    public class CharacterParameterModelFactory 
    {
        public CharacterParameterModel Create ()
        {
            return new CharacterParameterModel();
        }

        public CharacterParameterModel CreateDummy ()
        {
            return new CharacterParameterModel (CharacterParameterModel.InitData);
        }
    }

}
