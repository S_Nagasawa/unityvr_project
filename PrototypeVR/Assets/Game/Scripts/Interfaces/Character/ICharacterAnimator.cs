﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces.Character
{
    /// <summary>
    /// CharacterクラスのActionを制約するクラス
    /// </summary>
    public interface ICharacterAnimator
    {
        /// <summary>
        /// 着地しているか
        /// </summary>
        bool IsGrounded { get; }

        /// <summary>
        /// しゃがんでいるか？
        /// </summary>
        bool IsCrouching { get; }

        /// <summary>
        /// AnimatorComponentをoverrideする
        /// </summary>
        void OnAnimatorMove();
    }


}


