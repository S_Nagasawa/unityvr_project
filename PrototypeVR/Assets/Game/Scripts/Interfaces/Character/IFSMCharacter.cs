﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces.Character
{

    /// <summary>
    /// Finite State Machineを管理するinterface
    /// </summary>
    public interface IFSMCharacter
    {
        /// <summary>
        /// FSMシステム初期化
        /// </summary>
        void FSMInitialize();

        /// <summary>
        /// FSMシステム削除
        /// </summary>
        void FSMDestroy();
    }

}


