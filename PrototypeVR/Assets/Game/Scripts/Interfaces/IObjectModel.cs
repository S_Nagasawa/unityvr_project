﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces
{

    /// <summary>
    /// CoreBehaviourのinterface
    /// instance、singletonは問わない
    /// </summary>
    public interface IObjectModel
    {
        /// <summary>
        /// CachedTransform
        /// </summary>
        Transform RootTransform { get; }

        /// <summary>
        /// BehaviourをDestroyする
        /// </summary>
        void DoDestroy ();

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize ();

        /// <summary>
        /// 初期化JOB
        /// </summary>
        IEnumerator Initializer();
    }


    /// <summary>
    /// Character、StaticMesh、UIがもつtriggerControllerのinterface
    /// </summary>
    public interface ITriggerEventModel
    {
        /// <summary>
        /// イベントハンドラController
        /// </summary>
        EventTriggerBase TriggerController { get; }
    }


    /// <summary>
    /// Character、StaticMesh、UIがもつinterface
    /// Playerが呼び、interfaceを継承しているComponentが何かをする
    /// </summary>
    public interface IGazenEvent
    {
        /// <summary>
        /// PointerEnter時に呼ばれる
        /// </summary>
        void ApplyGazenPointerEnter ();

        /// <summary>
        /// PointerがHoldされている時に呼ばれる
        /// </summary>
        void ApplyGazenPointerHold ();

        /// <summary>
        /// PointerEnter時に呼ばれる *Player引数付き
        /// </summary>
        void ApplyGazenPointerEnter (ObjectBehaviour arg);

        /// <summary>
        /// PointerがHoldされている時に呼ばれる *Player引数付き
        /// </summary>
        void ApplyGazenPointerHold (ObjectBehaviour arg);

        /// <summary>
        /// IGazenHoldが呼ばれたか？
        /// すでに呼ばれている場合は処理しない
        /// </summary>
        bool IsApplyGazenEventHold { get; }

        /// <summary>
        /// IGazenPointerEnterが呼ばれたか？
        /// すでに呼ばれている場合は処理しない
        /// </summary>
        bool IsApplyGazenEventEnter { get; }
    }


}


