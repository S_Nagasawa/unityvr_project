﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UniRx;

using Game.Interfaces;
using Game.Enums;

using Extension   = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Interfaces.Event
{

    /// <summary>
    /// Eventドリブンinterface
    /// </summary>
    public interface IEventTrigger
    {
        /// <summary>
        /// Eventドリブン
        /// </summary>
        EventTrigger Trigger { get; }

        /// <summary>
        /// Lockされたか？
        /// </summary>
        bool IsLock { get; }

        /// <summary>
        /// Holdされたか？
        /// </summary>
        bool IsHold { get; }
    }
}
