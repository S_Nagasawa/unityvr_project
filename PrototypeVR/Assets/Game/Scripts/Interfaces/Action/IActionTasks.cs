﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Interfaces.Action;
using Game.Events;
using Game.Enums;
using Game.State;
using Game.Spawn;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Action
{

    /// <summary>
    /// Characterのタスクinterface
    /// </summary>
    public interface IActionTasks
    {
        /// <summary>
        /// 次のタスクの処理をする
        /// </summary>
        void ThinkNextTask();

        /// <summary>
        /// 前のタスクの処理をする
        /// </summary>
        void ThinkPreviousTask();

        /// <summary>
        /// 全てのタスクを完了させる
        /// </summary>
        void AllTaskDone ();

        /// <summary>
        /// 全てのタスクをリセットする
        /// </summary>
        void ResetTaskDone ();
    }

}


