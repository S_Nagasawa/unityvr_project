﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Events;
using Game.Enums;
using Game.State;
using Game.Spawn;
using UniRx.Triggers;
using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Interfaces.Action
{
    /// <summary>
    /// Characterの行動パターン
    /// </summary>
    public interface IActionMethod
    {
        /// <summary>
        /// 待ち
        /// </summary>
        void Idle();

        /// <summary>
        /// 歩く
        /// </summary>
        void Walk();

        /// <summary>
        /// 走る
        /// </summary>
        void Run();

        /// <summary>
        /// 飛ぶ
        /// </summary>
        void Jump ();

        /// <summary>
        /// 物をとる
        /// </summary>
        void Pick ();

        /// <summary>
        /// 構える
        /// </summary>
        void Aim ();

        /// <summary>
        /// ターゲットの更新
        /// </summary>
        void SetLockTarget (ObjectBehaviour obj);

        /// <summary>
        /// ターゲット
        /// </summary>
        ObjectBehaviour LockTarget { get; }
    }
        

}


