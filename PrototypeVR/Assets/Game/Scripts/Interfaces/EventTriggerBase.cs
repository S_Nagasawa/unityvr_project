﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UniRx;
using UniRx.Triggers;

using Game.Interfaces;
using Game.Interfaces.Event;
using Game.Enums;

using Extension   = Game.Lib.MethodExtensionForMonoBehaviour;
using UIExtension = Game.Lib.UIBehaviourExtensions;



namespace Game.Interfaces
{


    /// <summary>
    /// イベントハンドラController
    /// Player以外のObjectBehaviourクラスに有効
    /// Playerに見られた時に発火する
    /// IGvrGazeResponderの実際の処理をする
    /// ObjectBehaviourの子供
    /// </summary>
    public abstract class EventTriggerBase : EventTriggerBehaviour, IEventTrigger, IGvrGazeResponder
    {
        /// <summary>
        /// Eventドリブン
        /// </summary>
        public virtual EventTrigger Trigger { get; protected set; }


        /// <summary>
        /// Lockされたか？
        /// </summary>
        public bool IsLock { get; protected set; }


        /// <summary>
        /// Holdされたか？
        /// </summary>
        public bool IsHold { get; protected set; }


        /// <summary>
        /// 自身のGameObject
        /// </summary>
        public virtual GameObject GO 
        {
            get 
            {
                if (RootTransform == null) 
                {
                    return gameObject;
                }
                return RootTransform.gameObject;
            }
        }


        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
        }


        /// <summary>
        /// 初期化
        /// </summary>
        public override void Initialize ()
        {
            Trigger = Extension.TryComponent<EventTrigger> (GO);
        }


        /// <summary>
        /// Raises the pointer enter event.
        /// </summary>
        /// <param name="data">Data.</param>
        public virtual void OnPointerEnter (BaseEventData data)
        {
        }


        /// <summary>
        /// Raises the pointer down event.
        /// </summary>
        /// <param name="data">Data.</param>
        public virtual void OnPointerDown (BaseEventData data)
        {
        }


        /// <summary>
        /// Raises the pointer exit event.
        /// </summary>
        /// <param name="data">Data.</param>
        public virtual void OnPointerExit (BaseEventData data)
        {
        }


        /// <summary>
        /// 視線が入った
        /// </summary>
        public virtual void OnGazeEnter ()
        {
            IsLock = true;
        }


        /// <summary>
        /// 視線から外れた
        /// </summary>
        public virtual void OnGazeExit ()
        {
            IsLock = false;
            IsHold = false;
        }


        /// <summary>
        /// 視線が固定されている
        /// </summary>
        public virtual void OnGazeHold ()
        {
            IsHold = true;
        }


        /// <summary>
        /// Raises the gaze trigger event.
        /// </summary>
        public virtual void OnGazeTrigger() 
        {
            Debug.Log("OnGazeTrigger");
        }



    }

}


