﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Spawn;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Statistic
{

    /// <summary>
    /// 静的なObjectの基底クラス
    /// </summary>
    public abstract class StaticObjectBehaviour : ObjectBehaviour
    {
        /// <summary>
        /// 静的Model
        /// </summary>
        public IStaticObjectModel StaticObjectModel { get; protected set; }


        /// <summary>
        /// Mesh
        /// </summary>
        public Renderer Renderer { get; protected set; }


        /// <summary>
        /// Playerが見ているか？
        /// </summary>
        public bool IsLock
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsLock;
            }
        }

        /// <summary>
        /// Playerが一定時間見ているか？
        /// </summary>
        public bool IsHold
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsHold;
            }
        }
            
        /// <summary>
        /// BehaviourをDestroyする
        /// 本クラスでは非破壊Objectになる
        /// </summary>
        public override void DoDestroy ()
        {
            return;
        }


        protected override void Awake ()
        {
            base.Awake ();
            if (StaticObjectModel == null) 
            {
                StaticObjectModel = new StaticObjectModelFactory ().Create (RootTransform.name);
            }
            StaticObjectModel.Initialize ();

            if (TriggerController == null) 
            {
                TriggerController = RootTransform.GetComponent<EventTriggerBase> ();
            }
            TriggerController.Initialize ();
        }
    }


}
