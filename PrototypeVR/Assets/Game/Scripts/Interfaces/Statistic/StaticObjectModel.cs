﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Interfaces;
using Game.Enums;
using Game.State;
using Game.Spawn;

using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Statistic
{

    /// <summary>
    /// 静的StaticObjectのinterface
    /// </summary>
    public interface IStaticObjectModel
    {
        /// <summary>
        /// 名前
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize();
    }


    /// <summary>
    /// IStaticObjectModel継承クラス
    /// </summary>
    public class StaticObjectModel : IStaticObjectModel
    {
        /// <summary>
        /// 名前
        /// </summary>
        public string Name { get; protected set; }


        public StaticObjectModel ()
        {
        }

        public StaticObjectModel(string name)
        {
            Name = name;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize()
        {}
    }        



    /// <summary>
    /// Static object model factory.
    /// </summary>
    public class StaticObjectModelFactory 
    {

        public StaticObjectModel Create ()
        {
            return new StaticObjectModel ();
        }

        public StaticObjectModel Create (string name)
        {
            return new StaticObjectModel (name);
        }
    }

}
