﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces
{

    /// <summary>
    /// Gameのコアの基底クラス
    /// ライブラリ以外のComponentは本クラスから派生
    /// MonoBehaviourは独自で持たない
    /// </summary>
    public abstract class CoreBehaviour : MonoBehaviour, IObjectModel
    {
        /// <summary>
        /// CachedTransform
        /// </summary>
        public Transform RootTransform { get; protected set; }


        protected virtual void Awake ()
        {
            RootTransform = transform;
        }

        /// <summary>
        /// BehaviourをDestroyする
        /// </summary>
        public virtual void DoDestroy () {}

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize () 
        {

        }

        /// <summary>
        /// 初期化JOB
        /// </summary>
        public virtual IEnumerator Initializer ()
        {
            yield break;
        }

    }

}

