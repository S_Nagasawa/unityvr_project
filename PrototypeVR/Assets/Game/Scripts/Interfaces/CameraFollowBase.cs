﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Interfaces.Cameras;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.StaticMesh;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces
{


    /// <summary>
    /// FPSカメラの基底クラス
    /// </summary>
    public abstract class CameraFollowBase : ObjectBehaviour, ICameraFollow
    {
        /// <summary>
        /// 対象となるカメラ
        /// </summary>
        /// <value>The camera.</value>
        public Camera Camera { get; protected set; }


        /// <summary>
        /// GoogleVR Pointer
        /// </summary>
        /// <value>The gvr reticle pointer.</value>
        public GvrBasePointer GvrReticlePointer { get; protected set; }


        /// <summary>
        /// Gets the gvr pointer raycaster.
        /// </summary>
        /// <value>The gvr pointer raycaster.</value>
        public GvrBasePointerRaycaster GvrPointerRaycaster { get; protected set; }


        /// <summary>
        /// PointerEventClass
        /// </summary>
        /// <value>The gvr gaze.</value>
        public GvrGaze GvrGazePointerController { get; protected set; }


        public float RayLength { get; protected set; }


        public int LayerMask { get; protected set; }


        /// <summary>
        /// PhysicsRayで捉えたターゲット
        /// UI以外になる
        /// </summary>
        public virtual ObjectBehaviour LockTarget { get; protected set; }


        protected override void Awake ()
        {
            base.Awake ();

            if (GvrPointerRaycaster == null) 
            {
                GvrPointerRaycaster = RootTransform.GetComponent<GvrBasePointerRaycaster> () as GvrPointerPhysicsRaycaster;
            }
            if (GvrGazePointerController == null) 
            {
                GvrGazePointerController = RootTransform.GetComponent<GvrGaze> ();
            }
            if (GvrReticlePointer == null) 
            {
                GvrReticlePointer = RootTransform.GetComponentInChildren<GvrBasePointer> ();
            }

            // 使わないのでnull
            {
                TriggerController = null;
            }
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="rayLength">Ray length.</param>
        /// <param name="layerMask">Layer mask.</param>
        public virtual void Initialize (float rayLength, int layerMask) 
        {
            RayLength = rayLength;
            LayerMask = layerMask;
        }


        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="rayLength">Ray length.</param>
        /// <param name="layerMask">Layer mask.</param>
        /// <param name="gazeHoldTime">Gaze hold time.</param>
        public virtual void Initialize (float rayLength, int layerMask, float gazeHoldTime)
        {
            RayLength = rayLength;
            LayerMask = layerMask;
            GvrGazePointerController.Setup (gazeHoldTime);
        }


        /// <summary>
        /// Cameraが捉えたObjectBehaviourを更新する
        /// Nullになる事もある
        /// </summary>
        /// <param name="arg">Argument.</param>
        protected virtual void SetLockTarget (ObjectBehaviour arg) 
        {
        }

    }


}


