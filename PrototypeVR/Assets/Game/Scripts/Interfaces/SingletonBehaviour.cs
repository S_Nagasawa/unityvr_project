﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces
{

    /// <summary>
    /// Singletonの基底クラス
    /// </summary>
    public abstract class SingletonBehaviour : CoreBehaviour
    {
        /// <summary>
        /// 成功した時に呼ばれる
        /// </summary>
        public virtual void SuccessCallback () {}

        /// <summary>
        /// 失敗した時に呼ばれる
        /// </summary>
        public virtual void ErrorCallback () {}

        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        /// <returns>The routine.</returns>
        public virtual IEnumerator JobRoutine ()
        {
            yield break;
        }

        /// <summary>
        /// システム構築済みかのチェック
        /// </summary>
        public virtual void SystemCreatedCheck () {}

        /// <summary>
        /// それぞれのタスクを実行する
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        /// <param name="interval">Interval.</param>
        public virtual IEnumerator JobRoutine(string sceneName, float interval) 
        {
            yield break;
        }
    }



}


