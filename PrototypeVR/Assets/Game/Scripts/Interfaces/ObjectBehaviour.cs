﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces
{

    /// <summary>
    /// Component、各種パラメーターを保持する基底クラス
    /// CoreBehaviourの派生
    /// EventTriggerBaseクラスでIGvrGazeResponderの実際の処理をする
    /// 本クラスはWrapperとなる
    /// </summary>
    public abstract class ObjectBehaviour : CoreBehaviour, ITriggerEventModel, IGazenEvent, IGvrGazeResponder
    {

        /// <summary>
        /// セットアップが完了したか？
        /// 派生クラスが問題なく使える状態であるかも確認
        /// </summary>
        public virtual bool IsSetUpDone { get; protected set; }

        /// <summary>
        /// イベントハンドラController
        /// </summary>
        public virtual EventTriggerBase TriggerController { get; protected set; }

        /// <summary>
        /// IGazenHoldが呼ばれたか？
        /// すでに呼ばれている場合は処理しない
        /// </summary>
        public bool IsApplyGazenEventHold { get; protected set; }

        /// <summary>
        /// IGazenPointerEnterが呼ばれたか？
        /// すでに呼ばれている場合は処理しない
        /// </summary>
        public bool IsApplyGazenEventEnter { get; protected set; }

        /// <summary>
        /// PointerEnter時に呼ばれる
        /// </summary>
        public virtual void ApplyGazenPointerEnter () 
        {
            IsApplyGazenEventEnter = true;
            IsApplyGazenEventHold  = false;
        }

        /// <summary>
        /// PointerがHoldされている時に呼ばれる
        /// </summary>
        public virtual void ApplyGazenPointerHold () 
        {
            IsApplyGazenEventHold = true;
        }

        /// <summary>
        /// PointerEnter時に呼ばれる *Player引数付き
        /// </summary>
        public virtual void ApplyGazenPointerEnter (ObjectBehaviour arg) 
        {
            IsApplyGazenEventEnter = true;
            IsApplyGazenEventHold  = false;
        }

        /// <summary>
        /// PointerがHoldされている時に呼ばれる *Player引数付き
        /// </summary>
        public virtual void ApplyGazenPointerHold (ObjectBehaviour arg) 
        {
            IsApplyGazenEventHold = true;
        }

        /// <summary>
        /// 視線が入った
        /// </summary>
        public virtual void OnGazeEnter ()
        {
            if (TriggerController == null) 
            {
                return;
            }
            TriggerController.OnGazeEnter ();
        }

        /// <summary>
        /// 視線から外れた
        /// </summary>
        public virtual void OnGazeExit ()
        {
            if (TriggerController == null) 
            {
                return;
            }
            TriggerController.OnGazeExit ();
            IsApplyGazenEventEnter = false;
            IsApplyGazenEventHold  = false;
        }

        /// <summary>
        /// 視線が固定されている
        /// </summary>
        public virtual void OnGazeHold ()
        {
            if (TriggerController == null) 
            {
                return;
            }
            TriggerController.OnGazeHold ();
        }

        /// <summary>
        /// Raises the gaze trigger event.
        /// </summary>
        public virtual void OnGazeTrigger() 
        {
            if (TriggerController == null) 
            {
                return;
            }
            TriggerController.OnGazeTrigger ();
        }
    }



}


