﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Item
{

    /// <summary>
    /// アイテムのinterface
    /// </summary>
    public interface IItem
    {
        /// <summary>
        /// アイテム名
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 個数
        /// </summary>
        int Amount { get; }

        /// <summary>
        /// アイテム種別
        /// </summary>
        byte Category { get; }

        /// <summary>
        /// アイテム種別
        /// </summary>
        ItemType ItemType { get; }

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize ();
    }


    /// <summary>
    /// アイテムの基底クラス
    /// </summary>
    public abstract class ItemBase : IItem 
    {
        /// <summary>
        /// アイテム名
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 個数
        /// </summary>
        public int Amount { get; protected set; }

        /// <summary>
        /// アイテム種別
        /// </summary>
        public ItemType ItemType 
        { 
            get { return (ItemType)Category; }
        }

        /// <summary>
        /// アイテム種別
        /// </summary>
        public byte Category { get; protected set; }

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize() {}


        public override string ToString ()
        {
            return string.Format ("[ItemBase: " +
                "Name={0}, " +
                "Amount={1}, " +
                "ItemType={2}, " +
                "Category={3}]", 
                Name, 
                Amount, 
                ItemType, 
                Category);
        }


        /// <summary>
        /// Amountを更新する
        /// </summary>
        public virtual void UpdateAmount (int amount)
        {
            Amount = amount;
        }

    }


}

