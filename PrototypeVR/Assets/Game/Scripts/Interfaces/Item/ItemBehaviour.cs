﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Item
{

    /// <summary>
    /// ItemBaseのComponentクラス
    /// Scene上に存在するitemの基底クラス
    /// </summary>
    public abstract class ItemBehaviour : ObjectBehaviour
    {

        /// <summary>
        /// アイテム
        /// </summary>
        public ItemBase ItemModel { get; protected set; }


        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake ()
        {
            base.Awake ();
        }


    }

}

