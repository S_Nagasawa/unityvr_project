﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Game.Interfaces;
using Game.Interfaces.Character;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.Events;
using Game.Singleton;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Item
{

    /// <summary>
    /// 拳銃、WeaponControllerのinterface
    /// </summary>
    public interface IWeaponModel
    {

        /// <summary>
        /// Bulletを消費する
        /// </summary>
        void Shot ();

        /// <summary>
        /// 装填する
        /// </summary>
        void Reload ();

        /// <summary>
        /// 構える
        /// </summary>
        void Equip ();

        /// <summary>
        /// 解除する
        /// </summary>
        void UnEquip ();
    }


}

