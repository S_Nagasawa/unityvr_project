﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


namespace Game.Interfaces
{

    /// <summary>
    /// Eventを管理するBehaviourクラス
    /// CoreBehaviourを継承
    /// Character、StaticMesh、UIのイベントドリブンを管理するクラス
    /// </summary>
    public abstract class EventTriggerBehaviour : CoreBehaviour
    {
    }        

}


