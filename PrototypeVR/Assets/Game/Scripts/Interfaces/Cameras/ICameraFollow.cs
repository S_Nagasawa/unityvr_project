﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

using Game.Interfaces;
using Game.Lib;
using Game.Enums;
using Game.State;
using Game.State.StateMachine;
using Game.Spawn;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Tag;
using Game.StaticMesh;
using UniRx;
using UniRx.Triggers;


using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.Cameras
{


    public interface ICameraFollow
    {
        /// <summary>
        /// 対象となるカメラ
        /// </summary>
        /// <value>The camera.</value>
        Camera Camera { get; }

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize (float rayLength, int layerMask);

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize (float rayLength, int layerMask, float gazeHoldTime);

        /// <summary>
        /// GoogleVR Pointer
        /// </summary>
        /// <value>The gvr reticle pointer.</value>
        GvrBasePointer GvrReticlePointer { get; }

        /// <summary>
        /// Gets the gvr pointer raycaster.
        /// </summary>
        /// <value>The gvr pointer raycaster.</value>
        GvrBasePointerRaycaster  GvrPointerRaycaster { get; }

        /// <summary>
        /// PointerEventClass
        /// </summary>
        /// <value>The gvr gaze.</value>
        GvrGaze GvrGazePointerController { get; }
    }


}


