﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;
using Game.Interfaces.Statistic;
using Game.Interfaces.Action;
using Game.Interfaces.Character;
using Game.Interfaces.UI;
using Game.Actor;
using Game.Actor.Systems;
using Game.Actor.Action;
using Game.Enums;
using Game.State;
using Game.Spawn;
using Game.Events;
using Game.Example;
using Game.Scene.Base.UI;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Game.Interfaces.Scene
{


    /// <summary>
    /// ISceneParamのinterface
    /// </summary>
    public interface ISceneParam
    {
        /// <summary>
        /// Scene切り替えの際のScene名
        /// </summary>
        string SceneName { get; }

        /// <summary>
        /// Scene切り替え前のCallback
        /// </summary>
        event System.Action OnLoadCallback;

        /// <summary>
        /// Scene切り替え後のCallback
        /// </summary>
        event System.Action UnLoadCallback;

        /// <summary>
        /// 初期化
        /// </summary>
        void Initialize();

        /// <summary>
        /// Scene名が定義されているか？
        /// </summary>
        bool NotExecuteScene();
    }


    /// <summary>
    /// SceneParameterの基底クラス
    /// </summary>
    public abstract class SceneParamBase : ISceneParam
    {
        /// <summary>
        /// Scene切り替えの際のScene名
        /// </summary>
        public virtual string SceneName { get; protected set; }

        /// <summary>
        /// Scene切り替え前のCallback
        /// </summary>
        public virtual event System.Action OnLoadCallback = delegate {};

        /// <summary>
        /// Scene切り替え後のCallback
        /// </summary>
        public virtual event System.Action UnLoadCallback = delegate {};

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize() {}

        /// <summary>
        /// Scene名が定義されているか？
        /// </summary>
        public bool NotExecuteScene ()
        {
            return string.IsNullOrEmpty (SceneName);
        }
    }



}

