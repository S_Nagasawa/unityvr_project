﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.UI
{
    /// <summary>
    /// IUIObjectModel継承クラス
    /// </summary>
    public class UIObjectModel : IUIObjectModel
    {
        /// <summary>
        /// 名前
        /// </summary>
        public string Name { get; protected set; }


        public UIObjectModel ()
        {
        }

        public UIObjectModel(string name)
        {
            Name = name;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public virtual void Initialize()
        {}
    }


    /// <summary>
    /// User interface object model factory.
    /// </summary>
    public class UIObjectModelFactory 
    {

        public UIObjectModel Create ()
        {
            return new UIObjectModel ();
        }

        public UIObjectModel Create (string name)
        {
            return new UIObjectModel (name);
        }
    }

}