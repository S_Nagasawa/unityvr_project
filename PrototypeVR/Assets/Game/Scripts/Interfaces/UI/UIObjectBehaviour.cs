﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;
using Game.Interfaces.Scene;
using Game.Enums;
using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.UI
{


    /// <summary>
    /// UIObjectの基底クラス
    /// Image、Button、などCanvas以外にAttachされる
    /// </summary>
    public abstract class UIObjectBehaviour : ObjectBehaviour
    {

        /// <summary>
        /// Occurs when on hold action.
        /// </summary>
        public virtual event System.Action OnHoldAction = delegate {};

        /// <summary>
        /// UIModel
        /// </summary>
        public IUIObjectModel UIObjectModel { get; protected set; }

        /// <summary>
        /// 自身のRectTransform Component
        /// </summary>
        public RectTransform RectTransform { get; protected set; }

        /// <summary>
        /// 自身のroot canvas
        /// </summary>
        public UIObjectRootBehaviour RootCanvas  { get; private set; }

        /// <summary>
        /// UI自身のType
        /// </summary>
        public UIType UIType { get; protected set; }

        /// <summary>
        /// Lockされているか？
        /// </summary>
        public bool IsLock
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsLock;
            }
        }

        /// <summary>
        /// Holdされているか？
        /// </summary>
        public bool IsHold
        {
            get 
            {
                if (TriggerController == null) 
                {
                    return false;
                }
                return TriggerController.IsHold;
            }
        }


        protected override void Awake ()
        {
            base.Awake ();

            if (UIObjectModel == null) 
            {
                UIObjectModel = new UIObjectModelFactory ().Create (RootTransform.name);
            }

            if (TriggerController == null) 
            {
                TriggerController = GetComponent<EventTriggerBase> ();
            }

            if (RootCanvas == null) 
            {
                RootCanvas = Extension.GetComponentInParentAndChildren<UIObjectRootBehaviour> (RootTransform);
            }

            if (RectTransform == null) 
            {
                RectTransform = GetComponent<RectTransform> ();
            }

            UIObjectModel.Initialize ();
            TriggerController.Initialize ();
        }


        /// <summary>
        /// UIActionを実行する
        /// </summary>
        public virtual void OnUIAction () {}


    }


}
