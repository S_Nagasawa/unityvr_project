﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.UI
{
    /// <summary>
    /// UIComponentのinterface
    /// </summary>
    public interface IUIComponents
    {
        /// <summary>
        /// 親のCanvas
        /// </summary>
        Canvas Canvas { get; }

        /// <summary>
        /// Canvas Pointer
        /// </summary>
        GvrBasePointerRaycaster PointerRayCaster { get; }

        /// <summary>
        /// FieldにいるCharacterList
        /// </summary>
        List<CharacterBehaviour> Characters { get; }

        /// <summary>
        /// セットアップが完了したか？
        /// </summary>
        bool IsSetUpDone { get; }

        /// <summary>
        /// Canvasに紐づくRectTransform
        /// </summary>
        RectTransform RectTransform { get; }
    }

}