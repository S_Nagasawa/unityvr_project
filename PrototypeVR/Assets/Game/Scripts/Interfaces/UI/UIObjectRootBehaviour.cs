﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.Interfaces;

using UniRx;
using UniRx.Triggers;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;

namespace Game.Interfaces.UI
{

    /// <summary>
    /// ugui Canvasに紐づく基底クラス
    /// Button ImageはUIObjectBehaviourが基底クラスになる
    /// UIObjectBehaviourがイベントドリブンを持つ
    /// </summary>
    public abstract class UIObjectRootBehaviour : ObjectBehaviour, IUIComponents 
    {

        /// <summary>
        /// 親のCanvas
        /// </summary>
        public Canvas Canvas { get; protected set; }

        /// <summary>
        /// Canvas Pointer
        /// </summary>
        public GvrBasePointerRaycaster PointerRayCaster { get; protected set; }

        /// <summary>
        /// FieldにいるCharacterList
        /// </summary>
        public List<CharacterBehaviour> Characters { get; protected set; }

        /// <summary>
        /// Canvasに紐づくRectTransform
        /// </summary>
        public RectTransform RectTransform { get; protected set; }

        /// <summary>
        /// UICanvasに一番近いCharacter
        /// </summary>
        /// <value>The lock target.</value>
        public CharacterBehaviour LockTarget { get; protected set; }


        protected override void Awake ()
        {
            base.Awake ();

            if (Canvas == null) 
            {
                Canvas = Extension.GetComponentInParentAndChildren<Canvas> (RootTransform);
            }

            if (PointerRayCaster == null) 
            {
                PointerRayCaster = Canvas.GetComponent<GvrBasePointerRaycaster> () as GvrPointerGraphicRaycaster;
            }

            if (RectTransform == null) 
            {
                RectTransform = Canvas.GetComponent<RectTransform> ();
            }
        }


        /// <summary>
        /// JOBを実行する
        /// </summary>
        protected virtual IEnumerator JobRoutine ()
        {
            yield break;
        }


        /// <summary>
        /// 座標から最短距離のSpawnObjectを取得して返す
        /// </summary>
        public virtual CharacterBehaviour GetMinDistanceCharacter ()
        {
            if (Extension.IsNullOrEmpty<CharacterBehaviour> (Characters)) 
            {
                return null;
            }

            return Characters
                .Where((CharacterBehaviour arg) => arg != null)
                .OrderBy((CharacterBehaviour arg) => (arg.RootTransform.position - RootTransform.position).magnitude)
                .FirstOrDefault();
        }


        /// <summary>
        /// Canvasを回転させる
        /// </summary>
        public virtual void ApplyTurnRotation ()
        {
        }


        /// <summary>
        /// Characterが一定の距離内か
        /// </summary>
        /// <param name="arg">Argument.</param>
        public virtual bool CanGazenUIEvent () 
        {
            if (LockTarget == null) 
            {
                return false;
            }
            float _distance = Vector3.Distance (RootTransform.position, LockTarget.RootTransform.position);
            if (_distance < LockTarget.UIDistance) 
            {
                return true;
            }
            return false;
        } 


        /// <summary>
        /// 回転軸をリセットする
        /// </summary>
        /// <returns>The turn rotation.</returns>
        /// <param name="que">Que.</param>
        /// <param name="interval">Interval.</param>
        public virtual IEnumerator InitialTurnRotation (Quaternion que, float interval)
        {
            yield break;
        }


    }


}
