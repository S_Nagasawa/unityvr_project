﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;
using Game.Scene.Base.UI;
using Game.Scene.Intro;
namespace Editors.UI
{


    [CustomEditor(typeof (SceneUIItemComponentBase))]
    public class SceneUIItemComponentHolder : Editor 
    {

        SceneUIItemComponentBase _Item = null;

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI ();
            _Item = target as SceneUIItemComponentBase;

            if (_Item.IsChangeScene) 
            {
                EditorGUILayout.HelpBox("SceneButton", MessageType.Info, true );
            }

            Debug.Log (_Item.IsChangeScene);
            EditorUtility.SetDirty (_Item);
        }
    }





}

