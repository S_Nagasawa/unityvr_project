﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

using Extension = Game.Lib.MethodExtensionForMonoBehaviour;


namespace Editors.Mesh
{


    [CustomEditor(typeof (MeshFilter))]
    public class MeshFilterCounter : Editor 
    {

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI ();
            MeshFilter fil  = target as MeshFilter;
            string polygons = "Triangles: " + fil.sharedMesh.triangles.Length/3;
            EditorGUILayout.LabelField( polygons );
        }

    }


    [CustomEditor(typeof(SkinnedMeshRenderer))]
    public class SkinnedMeshCounter : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            SkinnedMeshRenderer skin = target as SkinnedMeshRenderer;
            string polygons = "Triangles: " + skin.sharedMesh.triangles.Length/3;
            EditorGUILayout.LabelField( polygons );
        }
    }


}

